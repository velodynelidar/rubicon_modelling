% FMCW lidar (Rubicon) dsp framework simulation
% Created 11/24/2021 9:58:07
% author: @xgao
% Velodyne Lidar

clc; close all; clear;

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)
fcMod = 250e6;          % modified carrier frequency to bypass high sampling rate    
bw = 1.4e9;             % bandwidth 1GHz
fs0 = 12e9;             % sampling rate of Tx and Rx signals
fs = 1e9;               % sampling rate, choice of range 1-3GHz
rangeMax = 100;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength

% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

numChirpCyc = 2.4;          % parameterize total chirp cycles
tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = 0.1*tUpChirp;         % up-chirp head non-linear portion
t3p = 0.1*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = 0.1*tDownChirp;       % down-chirp head non-linear portion
t3n = 0.1*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% FFT parameter
fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K

disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])
% Range - Delay
range = 80;                % set range[m] to estimate
tDelay = range*2/c; 
disp(['Range = ', num2str(range), 'm']);
% Velocity - Doppler shift
v = -50;                     % speed of target in m/s (1m/s = 2.236936mi/hr)
fd = 2*v*fc/c;              % Doppler shift
if v == 0
    disp('Velocity = 0, Stationary target')
    elseif v > 0
    disp(['Velocity approaching = ', num2str(v), 'm/s (', num2str(v*2.236936), 'miles/hr)'])
    else
    disp(['Velocity leaving = ', num2str(v), 'm/s (', num2str(v*2.236936), 'miles/hr)'])
end

%% Waveform definition
t = 0:1/fs0:tMax-1/fs0;    
% ---------------------------------------
% Create Tx signal
% ---------------------------------------
% Frequency vs. time
fChirp = fcMod + bw*triangle(2*pi*ftotalChirp*t);    
% fChirp = fc;
% Integrate frequency by subinterval to get phase vs. time
N = length(fChirp);
phiChirp = zeros(1,N);
phiChirp(1) = 2*pi*fChirp(1);
for i = 2:N
    phiChirp(i) = phiChirp(i-1) + 2*pi*(fChirp(i)+fChirp(i-1))/2/fs0;
end

sigTx = cos(phiChirp);
% Transmit signal waveform
figure
subplot(421)
plot(t, fChirp/1e9)
xlabel('Time (s)'); ylabel('Frequency (GHz)'); title('Tx Frequency chirp'); grid on
subplot(423)
plot(t, phiChirp)
xlabel('Time (s)'); ylabel('Phase (Rad)'); title('Tx Phase (integration on freq subintervals)'); grid on
subplot(425)
plot(t, sigTx)
xlabel('Time (s)'); ylabel('Amplitude (v)'); title('Tx FMCW signal')
subplot(427)
spectrogram(sigTx,1024,1020,1024,fs0,'yaxis')
title('Tx FMCW Spectrogram (for verification)');colorbar('hide')

% ---------------------------------------
% Create Rx signal
% ---------------------------------------

% 1. Delay by # samples on time series
% delay_t = t0p;    % delayed by max range roundtrip time
% delay_num = ceil(delay_t/(1/fs0));

% Delay attempt 1 - zero out waveform head
% sigRx = zeros(1,length(sigTx));
% sigRx(delay_num+1:end) = sigTx(1:end-delay_num); % delayed Tx to create Rx
% fChirp_delayed = zeros(1,length(sigTx));
% fChirp_delayed(delay_num+1:end) = fChirp(1:end-delay_num); % delayed fChirp to create Rx frequency (for illustration only)

% Delay attempt 2 - circular shift
% sigRx = circshift(sigTx, delay_num);
% fChirp_delayed = circshift(fChirp, delay_num); 
% figure
% subplot(211); plot(t, fChirp/1e9); hold on; plot(t, fChirp_delayed/1e9);plot(t, (fChirp-fChirp_delayed)/1e9,'--')
% xlabel('Time (s)'); ylabel('Frequency (GHz)'); title('Tx vs. Rx Frequency chirp'); grid on
% legend('Tx frequency chirp', 'Rx frequency chirp','Frequency difference')
% subplot(212); plot(t, sigTx); hold on; plot(t, sigRx);
% xlabel('Time (s)'); ylabel('Amplitude (v)'); title('Tx vs. Rx FMCW signal')
% legend('Tx signal', 'Rx signal')

% 2. Delay by time on waveform (Note: this matches circular shift above)

fChirp_delayed = fcMod + bw*triangle(2*pi*ftotalChirp*(t-tDelay))  + fd; 
N = length(fChirp_delayed);
phiChirp_delayed = zeros(1,N);
phiChirp_delayed(1) = 2*pi*fChirp_delayed(1);
for i = 2:N
    phiChirp_delayed(i) = phiChirp_delayed(i-1) + 2*pi*(fChirp_delayed(i)+fChirp_delayed(i-1))/2/fs0;
end
% Complex Rx signal as IQ outputs
sigRx = cos(phiChirp_delayed); % single channel

subplot(422); 
plot(t, fChirp/1e9);hold on; plot(t, fChirp_delayed/1e9); plot(t, (fChirp-fChirp_delayed)/1e9,'--')
xlabel('Time (s)'); ylabel('Frequency (GHz)'); title('Tx vs. Rx Frequency chirp'); grid on
legend('Tx frequency chirp', 'Rx frequency chirp','Frequency difference')
subplot(424)
plot(t, phiChirp_delayed,'color',[0.85,0.33,0.10]); 
xlabel('Time (s)'); ylabel('Phase (Rad)'); title('Rx Phase'); grid on
subplot(426)
plot(t, sigRx);
xlabel('Time (s)'); ylabel('Amplitude (v)'); title('Rx FMCW signals');
subplot(428)
spectrogram(sigRx,1024,1020,1024,fs0,'yaxis')
title('Rx FMCW Spectrogram (for verification)');colorbar('hide')

%% Mixing and LPF
sigMx = sigTx .* conj(sigRx);
% Design a 8th-order lowpass Butterworth filter w/ fCutoff.
fCutoff = rangeMax*2/c * alpha_up * 1.1; % cutoff frequency defined as 1.x times of max beat freq
[b,a] = butter(8,fCutoff/(fs0/2));
% freqz(b,a)
sigMxFltr = filter(b,a,sigMx);

figure
subplot(321)
plot(t, real(sigMx), 'color', [0.4940 0.1840 0.5560]); 
xlabel('Time (s)'); ylabel('Amplitude (v)'); title('Mixed signal')
subplot(322)
spectrogram(sigMx,1024,1020,1024,fs0,'yaxis')
title('Mixed Signal Spectrogram (for verification)');colorbar('hide')
% ylim([0 0.5])
subplot(323)
plot(t, sigMxFltr, 'color', [0.4940 0.1840 0.5560]); 
xlabel('Time (s)'); ylabel('Amplitude (v)'); title('After filtering')
subplot(324)
spectrogram(sigMxFltr,1024,1020,1024,fs0,'yaxis')
title('Spectrogram (for verification)');colorbar('hide')
ylim([0 0.5])
%% Downsample 
dnspRate = fs0/fs;
% downSampleRate = 4;
% sigMxFilteredDownsampled = downsample(sigMxFiltered,downSampleRate);
% tDownsampled = downsample(t,downSampleRate);
sigMxFltrDnsp = sigMxFltr(1:dnspRate:end);
tDnsp = t(1:dnspRate:end);
disp(['Downsampled by ', num2str(dnspRate), '(', num2str(fs0/1e9),'GSps->', num2str(fs/1e9), 'GSps)'])

subplot(325)
plot(tDnsp, sigMxFltrDnsp, 'color', [0.4940 0.1840 0.5560]); 
xlabel('Time (s)'); ylabel('Amplitude (v)'); title(['After filtering & downsample (by ',num2str(dnspRate),')'])
subplot(326)
% spectrogram(sigMxFltrDnsp,1024/dnspRate,1020/dnspRate,1024/dnspRate,fs,'yaxis')
spectrogram(sigMxFltrDnsp,1024,1020,1024,fs,'yaxis')
title('Spectrogram (for verification)');colorbar('hide')
% ylim([0 0.5])

%% Perform segmented FFT, calculate range and velocity

% converting delay time to samples
t0pSps = round(t0p*fs);
t1pSps = round(t1p*fs);
t2pSps = round(t2p*fs); % fft up-chirp
t3pSps = round(t3p*fs);
t0nSps = round(t0n*fs);
t1nSps = round(t1n*fs);
t2nSps = round(t2n*fs); % fft down-chirp
t3nSps = round(t3n*fs);
tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

mkrFFTupS = t0pSps+t1pSps+1;
mkrFFTupE = mkrFFTupS+t2pSps;
mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
mkrFFTdnE = mkrFFTdnS+t2nSps;

delaySpsStartup = t0pSps+t1pSps;
delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

numSpsTotal = length(sigMxFltrDnsp);
numSpsFFT = fftSize;
fp = [];
fn = [];
rangeEst = [];
vEst = [];
fftBlkUpChirp = [];
fftBlkDnChirp = [];
fftBlkUpChirpIdx = [];
fftBlkDnChirpIdx = [];
cycleIdx = 1:tSpsFullCycle:numSpsTotal;

% Verification plot - Highlight delayed and valid FFT regions for verification
figure(4)
subplot(211)
plot(t, fChirp/1e9);hold on; plot(t, fChirp_delayed/1e9); plot(t, (fChirp-fChirp_delayed)/1e9,'--')
xlabel('Time (s)'); ylabel('Frequency (GHz)'); title('Tx vs. Rx Frequency chirp'); grid on
subplot(212)
plot(tDnsp,sigMxFltrDnsp, 'color', [0.4940 0.1840 0.5560]); hold on;
xlabel('Time (s)'); ylabel('Amplitude (v)'); title('Mixed signal with highlighted FFT regions')

for i = 1:length(cycleIdx)-1
    sigMxFltrDnsp_subSet = sigMxFltrDnsp(cycleIdx(i):cycleIdx(i+1)-1);
    for j = 1:length(sigMxFltrDnsp_subSet)
        if j >= mkrFFTupS && j < mkrFFTupE
            fftBlkUpChirp = [fftBlkUpChirp sigMxFltrDnsp_subSet(j)];
            fftBlkUpChirpIdx = [fftBlkUpChirpIdx cycleIdx(i)+j];
        elseif j >= mkrFFTdnS && j < mkrFFTdnE
            fftBlkDnChirp = [fftBlkDnChirp sigMxFltrDnsp_subSet(j)];
            fftBlkDnChirpIdx = [fftBlkDnChirpIdx cycleIdx(i)+j];
        end
    end
        figure(4)
        subplot(212)
        plot(tDnsp(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end)), fftBlkUpChirp,'color',[0.85,0.33,0.10]);
        plot(tDnsp(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end)), fftBlkDnChirp,'color',[0.85,0.33,0.10]);
        legend('Mixed signal', 'FFT region')      
        
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));

        
        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fs;                 % convert in frequency
        figure(5)
        subplot(floor(numChirpCyc),1,i)
        plot(f, abs(yFFTupChirp)); hold on; plot(f,abs(yFFTdnChirp));
        xlabel('Frequency (Hz)'); ylabel('Magnitude')
        
        
        [fpMag, fpIdx] = max(abs(yFFTupChirp));
        [fnMag, fnIdx] = max(abs(yFFTdnChirp));
        fpCur = f(fpIdx);
        fnCur = f(fnIdx);
        subplot(floor(numChirpCyc),1,i)
        plot(fpCur, fpMag,'g*'); text(fpCur, fpMag,['fp = ', num2str(fpCur/1e6),'MHz'])
        plot(fnCur, fnMag,'g^'); text(fnCur, fnMag,['fn = ', num2str(fnCur/1e6), 'MHz'])
        legend('Up-chirp FFT', 'Down-chirp FFT','Up-chirp peak', 'Down-chirp peak')
        
        rangeCur = (fpcur - fnCur)*c/4/alpha_up;
        vCur = -(fnCur + fpCur)*lambda/4;
        % Logging estimation results
        fp = [fp fpCur];
        fn = [fn fnCur];
        rangeEst = [rangeEst rangeCur];
        vEst = [vEst vCur];
        
        fftBlkUpChirp = [];
        fftBlkDnChirp = [];
        fftBlkUpChirpIdx = [];
        fftBlkDnChirpIdx = [];
end
disp('--------Estimation Results--------')
disp(['Estimated range is ', num2str(rangeEst), 'm'])
disp(['Estimated velocity is ', num2str(vEst), 'm/s'])
