% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% author: @xgao
% Velodyne Lidar

clc; close all; clear;

%% Load data
rangeRef = [1.8 1.8 10 10 1.4 1.4]; 
n = input('Enter a data set number: ');

switch n
    case 1
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_Trigger.mat';
        opticalPathDly = 6;
    case 2
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_I.mat'; % added delay line 1m
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_Trigger.mat';
        opticalPathDly = 6 + 6;
    case 3
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_Trigger.mat';
        opticalPathDly = 6;
    case 4
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_I.mat'; % added delay line 20m
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_Trigger.mat';
        opticalPathDly = 6+60;
    otherwise
        disp('Wrong entry, please choose between 1 and 4')
end
disp(['Reference range is ', num2str(rangeRef(n)), 'm'])

load (filenameI)
load (filenameQ)
load (filenameTrigger)

%% Initialize data
% Integrate drive signal by subinterval to get chirp vs. time
Fs = 1e9;
N = length(Ch4s);
driveSig = Ch4s; 
DriveSigIntgral = zeros(1,N);
DriveSigIntgral(1) = 2*pi*driveSig(1);
for i = 2:N
    DriveSigIntgral(i) = DriveSigIntgral(i-1) + 2*pi*(driveSig(i)+driveSig(i-1))/2/Fs;
end


t = 0:1/Fs:(N-1)/Fs; 
figure
subplot(211)
plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
subplot(212)
plot(t*1e6, DriveSigIntgral); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive chirp')

[minVal, tLaser] = min(DriveSigIntgral);
if tLaser > N/2
   [minVal, tLaser] = min(-DriveSigIntgral);
end

I_Raw = Ch1s;
Q_Raw = Ch2s;

figure
subplot(221)
plot(t*1e6, I_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel I)')
subplot(222); spectrogram(I_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

subplot(223)
plot(t*1e6, Q_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel Q)')
subplot(224); spectrogram(Q_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.5e9;             % bandwidth 1.5GHz
fs = 1e9;               % sampling rate, choice of range 1-3GHz
rangeMax = 100;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
sampleCropRate = 0.1;       % portion cropped at head and tail of chirp

% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = sampleCropRate*tUpChirp;         % up-chirp head non-linear portion
t3p = sampleCropRate*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = sampleCropRate*tDownChirp;       % down-chirp head non-linear portion
t3n = sampleCropRate*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% FFT parameter
fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K

disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])
disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])
%% Perform segmented FFT, calculate range and velocity
sigMx = I_Raw + 1i*Q_Raw;
sigMxSeg = sigMx(tLaser:end);
tSeg = t(tLaser:end);
numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/Fs;

% converting delay time to samples
t0pSps = round(t0p*fs);
t1pSps = round(t1p*fs);
t2pSps = round(t2p*fs); % fft up-chirp
t3pSps = round(t3p*fs);
t0nSps = round(t0n*fs);
t1nSps = round(t1n*fs);
t2nSps = round(t2n*fs); % fft down-chirp
t3nSps = round(t3n*fs);
tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

mkrFFTupS = t0pSps+t1pSps+1;
mkrFFTupE = mkrFFTupS+t2pSps;
mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
mkrFFTdnE = mkrFFTdnS+t2nSps;

delaySpsStartup = t0pSps+t1pSps;
delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

numSpsTotal = length(sigMxSeg);
numSpsFFT = fftSize;

% fp = [];
% fn = [];
% rangeEst = [];
% vEst = [];
% fftBlkUpChirp = [];
% fftBlkDnChirp = [];
% fftBlkUpChirpIdx = [];
% fftBlkDnChirpIdx = [];
cycleIdx = 1:tSpsFullCycle:numSpsTotal;
cycleLen = length(cycleIdx)-1;

% Verification plot - Highlight delayed and valid FFT regions for verification
% figure(4)
% subplot(211)
% plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
% subplot(212)
% plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')
fp = zeros(1,cycleLen); % pre-allocate space, speeds things up
fn = zeros(1,cycleLen);
fpMag = zeros(1,cycleLen);
fnMag = zeros(1,cycleLen);
rangeEst = zeros(1,cycleLen); % pre-allocate space, speeds things up
vEst = zeros(1,cycleLen); 
for i = 1:length(cycleIdx)-1
    fftBlkUpChirp = zeros(1,t2pSps); % pre-allocate space for fft chunks (total len = 3333)
    fftBlkDnChirp = zeros(1,t2nSps);
    fftBlkUpChirpIdx = zeros(1,t2pSps);
    fftBlkDnChirpIdx = zeros(1,t2nSps); 
    RunningCtrUp = 1;
    RunningCtrDn = 1;
    sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
    for j = 1:length(sigMx_subSet)
        if j >= mkrFFTupS && j < mkrFFTupE
            fftBlkUpChirp(RunningCtrUp) = sigMx_subSet(j);
            fftBlkUpChirpIdx(RunningCtrUp) = cycleIdx(i)+j-1;
            RunningCtrUp = RunningCtrUp + 1;
        elseif j >= mkrFFTdnS && j < mkrFFTdnE
            fftBlkDnChirp(RunningCtrDn) = sigMx_subSet(j);
            fftBlkDnChirpIdx(RunningCtrDn) = cycleIdx(i)+j-1;        
            RunningCtrDn = RunningCtrDn + 1;
        end
    end
%         figure(4)
%         subplot(211)
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, real(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - I', 'FFT region')
%         subplot(212)
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, imag(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, imag(fftBlkUpChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - Q', 'FFT region')        
        
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));
        
        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fs;                 % convert in frequency
        
        figure(5) % original spectrum
%         subplot(floor(numChirpCyc),1,i)
        plot(f/1e6, abs(yFFTupChirp)); hold on; plot(f/1e6,abs(yFFTdnChirp)); 
        xlabel('Frequency (MHz)'); ylabel('Magnitude')
        
        [fpMag, fpIdx] = max(abs(yFFTupChirp));
        [fnMag, fnIdx] = max(abs(yFFTdnChirp));
        fpCur = f(fpIdx);
        fnCur = f(fnIdx);

        rangeCur = ((fpCur - fnCur)*c/2/alpha_up - opticalPathDly)/2; % considering optical length differences
        vCur = -(fpCur + fnCur)*lambda/4;
        % Logging estimation results
%         fp = [fp fpCur];
%         fn = [fn fnCur];
        fp(i) = f(fpIdx);
        fn(i) = f(fnIdx);

%         rangeEst = [rangeEst rangeCur];
%         vEst = [vEst vCur];
        rangeEst(i) = rangeCur;
        vEst(i) = vCur;

        % Investigation on Masking         
        figure(6) % stack FFTs 
        subplot(211)
        plot(f/1e6, abs(yFFTupChirp)); hold on;
        xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Up-chirp FFT of all cycles')
%         xlim([-1e8 1e8])
        subplot(212)
        plot(f/1e6,abs(yFFTdnChirp)); hold on;
        xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Down-chirp FFT of all cycles')
%         xlim([-1e8 1e8])
        
%         figure(7) % converted spectrum to show range
% %         subplot(floor(numChirpCyc),1,i)
%         plot((f/alpha_up*c-6)/2, abs(yFFTupChirp)); hold on; plot((f/alpha_up*c-6)/2,abs(yFFTdnChirp));
%         xlabel('Range (m)'); ylabel('Magnitude')
% %         subplot(floor(numChirpCyc),1,i)
%         plot((fpCur/alpha_up*c-6)/2, fpMag,'g*'); text(fpCur/alpha_up*c/2, fpMag,['Up-chirp est range = ', num2str(fpCur/alpha_up*c/2),'m'])
%         plot((fnCur/alpha_up*c-6)/2, fnMag,'g^'); text(fnCur/alpha_up*c/2, fnMag,['Down-chirp est range = ', num2str(fnCur/alpha_up*c/2), 'm'])
%         legend('Up-chirp FFT conv range', 'Down-chirp FFT conv range','Up-chirp est range', 'Down-chirp est range')
        
        fftBlkUpChirp = [];
        fftBlkDnChirp = [];
        fftBlkUpChirpIdx = [];
        fftBlkDnChirpIdx = [];
end
disp('--------Estimation Results--------')
disp(['Estimated range is ', num2str(rangeEst), 'm'])
disp(['Estimated velocity is ', num2str(vEst), 'm/s'])
disp(['Mean range is ', num2str(mean(rangeEst)), 'm'])
disp(['Mean velocity is ', num2str(mean(vEst)), 'm/s'])
disp(['Mean, Median, std fp is ', num2str(mean(fp)/1e6), ' ', num2str(median(fp)/1e6), ' ', num2str(std(fp)/1e6), ' MHz'])
disp(['Mean, Median, std fn is ', num2str(mean(fn)/1e6), ' ', num2str(median(fn)/1e6), ' ', num2str(std(fn)/1e6),' MHz'])

%% Distribution plots
% Histogram of beat tone
edgesBeatTone = -500:10:500; % 10MHz wide bins
% edges = [-25:0.5:25]; % 10MHz wide bins
figure
subplot(211);histogram(fp/1e6,edgesBeatTone,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Upchirp FFT beat tone distribution')
subplot(212); histogram(fn/1e6,edgesBeatTone,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Downchirp FFT beat tone distribution')
