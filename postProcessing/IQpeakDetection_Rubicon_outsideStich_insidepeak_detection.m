clear all
close all
clc
% addpath('C:\Users\lhua\Box\Matlab Program\Functions')
% filepath='C:\Users\lhua\Box\Data\Rubicon\01122022\25M\';
filepath = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022';
% load( 'C:\Users\lhua\Box\Rubicon\data\XYScan_I')
% load( 'C:\Users\lhua\Box\Rubicon\data\XYScan_Q')
% load( 'C:\Users\lhua\Box\Rubicon\data\XYScan_Trigger')
% load( 'C:\Users\lhua\Box\Rubicon\data\XYScan_Y')
% load( 'C:\Users\lhua\Box\Rubicon\data\XYScan_X')
%% 
% n = size(Ch1);


% Y = reshape(Ch3s',1,n(1)*n(2));
% T = reshape(Ch4s',1,n(1)*n(2));
%% 
Fs = 1e9;
% time = 1/Fs*[1:length(T)]/1e-3; %ms
% offset_L = (1+1+2)*1.5;
 offset_L = 3;
 count = 0;
 num = 2.^12;
 frequency = Fs/num*[-num/2:num/2-1];

%49
%  gamma_up = 1.4160e+09/5e-6;
%  gamma_down = 1.4551e+09/5e-6;
gamma_up = 1.7090e+09/5e-6;
gamma_down = 1.7090e+09/5e-6;
 offset_f_up = offset_L/3e8*gamma_up;
 offset_f_down = offset_L/3e8*gamma_down;
 mask_band = (15e6/(Fs/num));
% mask_band = 0;
mask_offset_up = round(offset_f_up/(Fs/num)/2);
%  delays = [367968;299343;278519;410338;301524;311325];
mask_up = [(num/2+mask_offset_up -round(mask_band/2)):(num/2+mask_offset_up +round(mask_band/2))];
mask_offset_down = -round(offset_f_down/(Fs/num)/2);
mask_down = [(num/2+mask_offset_down -round(mask_band/2)):(num/2+mask_offset_down +round(mask_band/2))];
%% 
for k = 1:30
    tic
    k
    load( strcat(filepath,'XYScan_I_',num2str(k)));
    load( strcat(filepath,'XYScan_Q_',num2str(k)));
    load( strcat(filepath,'XYScan_Trigger_',num2str(k)));
    load( strcat(filepath,'XYScan_Y_',num2str(k)));
    load( strcat(filepath,'XYScan_X_',num2str(k)));
    load( strcat(filepath,'XYScan_X2_',num2str(k)));
    toc
    %% find delays among scopes
    Y = Ch7;
    Ch7 = [];
    X = Ch8;
    Ch8 = [];
    X2 = Ch3;
    Ch3 = [];
%     figure
%     plot(X);
%     hold on
%     plot(X2);
    delay_scopes(k) = finddelay(X2,X);
    if delay_scopes(k)<0
        delay_scopes(k) = finddelay(X,X2);
    end
%     delay_scopes(k)=0;
    X_shift = circshift(X,-delay_scopes(k));
    Y_shift = circshift(Y,-delay_scopes(k));
    %% 
    I_LDL = Ch1(1:end-delay_scopes(k))';
    Q_LDL = Ch2(1:end-delay_scopes(k))';
    Ch1 = [];
    Ch2 = [];
    ChIQ = I_LDL + i*Q_LDL; 
%     T = Ch4(1:end- delay_scopes(k))';
    T = Ch4(1:end- 2*delay_scopes(1))';
    Ch4 = [];
    X_shift_Chop = X_shift(1:end-delay_scopes(k))';
    Y_shift_Chop = Y_shift(1:end-delay_scopes(k))';
    X_shift = [];
    Y_shift = [];
    
    %% find start time
    diff_T =diff(T);

    [maxtab, mintab]=peakdet(diff_T, 0.1);
    maxerror = find(maxtab(:,2) < 0.1);
    maxtab(maxerror,:) = [];
    minerror = find(mintab(:,2) > -0.1);
    mintab(minerror,:) = [];    
    T_down_num = mintab(1:end,1)+1;
    T_up_num = maxtab(1:end,1)+1;
    fft_num = min(length(T_up_num),length(T_down_num))-1;
    

%% FFT
%  num = 2^12;


 %% FFT Range
 slope_range =  abs(T_down_num(1)-T_up_num(1));
 cutoff(1) = round((slope_range - 3333)/2);
 cutoff(2) =  round((slope_range - 2048)/2);
 %% 
 h = 1;
tic
 if T_up_num(1)<T_down_num(1)
    for k = 1:fft_num
            spec_down_raw = fftshift(ifft(ChIQ(T_down_num(k)+cutoff(h):T_up_num(k+1)-cutoff(h)),num))/length(T_down_num(k)+cutoff(h):T_up_num(k+1)-cutoff(h));
            spec_down = spec_down_raw;
            spec_down(mask_down) = 0;
            [peakval_down(count+k),peakloc_down(count+k)] = max(abs(spec_down));
            spec_up_raw = fftshift(ifft(ChIQ(T_up_num(k)+cutoff(h):T_down_num(k)-cutoff(h)),num))/length(T_down_num(k)+cutoff(h):T_up_num(k+1)-cutoff(h));
            spec_up = spec_up_raw;
            spec_up(mask_up) = 0;
            [peakval_up(count+k),peakloc_up(count+k)] = max(abs(spec_up));
            X_down(count+k) = X_shift_Chop(round( mean(T_up_num(k)+cutoff(h),T_up_num(k+1)-cutoff(h))));
            Y_down(count+k) = Y_shift_Chop(round( mean(T_up_num(k)+cutoff(h),T_up_num(k+1)-cutoff(h))));
%             t(count+k) = time(T_up_num(k)+cutoff(h)+num/2);
    end
 else
     for k = 1:fft_num
            spec_down_raw = fftshift(ifft(ChIQ(T_down_num(k)+cutoff(h):T_up_num(k)-cutoff(h)),num))/length(T_down_num(k)+cutoff(h):T_up_num(k)-cutoff(h));
            spec_down = spec_down_raw;
            spec_down(mask_down) = 0;
            [peakval_down(count+k),peakloc_down(count+k)] = max(abs(spec_down));
            spec_up_raw = fftshift(ifft(ChIQ(T_up_num(k)+cutoff(h):T_down_num(k+1)-cutoff(h)),num))/length(T_down_num(k)+cutoff(h):T_up_num(k)-cutoff(h));
            spec_up = spec_up_raw;
            spec_up(mask_up) = 0;
            [peakval_up(count+k),peakloc_up(count+k)] = max(abs(spec_up));
            X_down(count+k) = X_shift_Chop(round( mean(T_down_num(k)+cutoff(h),T_down_num(k+1)-cutoff(h))));
            Y_down(count+k) = Y_shift_Chop(round( mean(T_down_num(k)+cutoff(h),T_down_num(k+1)-cutoff(h))));
%             t(k) = time(T_up_num(k)+cutoff(h)+num/2);
    end
 end
 count = count+fft_num;
 toc
end 
%% peak detection
figure
plot(peakloc_up,peakval_up,'o')
% 
% figure
% plot(peakloc_down,peakval_down,'o')
% %up chirp
k = 1;
threshold = 4E-7;
% threshold = 0;
n = size(X_down);


% S = spec_up';
% n = size(S);
% mask_band = (15e6/(Fs/num));
% % mask_band = 0;
% mask_offset_up = round(offset_f_up/(Fs/num)/2);
% 
% mask = [(num/2+mask_offset_up -round(mask_band/2)):(num/2+mask_offset_up +round(mask_band/2))];
% 
% S_search = S;
% S_search (mask,:) = 0;
for h = 1:n(2)
%     [powerval_up(h),powerloc_up(h)] = max(abs(S(:,h)));
%      [peakval_up(h),peakloc_up(h)] = max(abs(S_search(:,h)));
     if peakval_up(h)<threshold;
%          peakloc_up(h) = num/2+1;
         beat_up((k-1)*n(2)+h) = NaN;
%          location_up((k-1)*n(2)+h) = NaN;
     else
         beat_up((k-1)*n(2)+h) = frequency(peakloc_up(h));
%          location_up((k-1)*n(2)+h) = distance_up(peakloc(h));
     end
end
% beat_up = beat;
% figure
% plot(location_up)
% ylabel('Distance')
% xlabel('Time')
% set(gca,'FontSize',30);
% hold on
%down chirp

% threshold = 3e-7;

% S =spec_down';
% n = size(S);
% % mask_band = (15e6/(Fs/num));
% mask_offset_down = -round(offset_f_down/(Fs/num)/2);
% % mask = [(num/2+mask_offset_up -round(mask_band/2)):(num/2+mask_offset_up +round(mask_band/2))];
% mask = [(num/2+mask_offset_down -round(mask_band/2)):(num/2+mask_offset_down +round(mask_band/2))];
% S_search = S;
% S_search (mask,:) = 0;
for h = 1:n(2)
%     [powerval_down(h),powerloc_down(h)] = max(abs(S(:,h)));
%      [peakval_down(h),peakloc_down(h)] = max(abs(S_search(:,h)));
     if peakval_down(h)<threshold
%          peakloc_down = num/2;
         beat_down((k-1)*n(2)+h) = NaN;
%          location_down((k-1)*n(2)+h) = NaN;
     else
         beat_down((k-1)*n(2)+h) = frequency(peakloc_down(h));
%          location_down((k-1)*n(2)+h) = distance_down(peakloc(h));
%          V((k-1)*n(2)+h) = 5;
     end
end
% beat_down = beat;
% figure
% plot(location_down)
% ylabel('Distance(m)')
% xlabel('Time')
% set(gca,'FontSize',30);
% 
%  distance_up = (frequency/gamma_up*3e8-offset_L)/2;

%  distance_down = (frequency/gamma_down*3e8+offset_L)/2;
offset_L = 6;
% location = ((beat_up/gamma_up*3e8-beat_down/gamma_down*3e8)/2-offset_L)/2;
location = ((beat_up-beat_down)/gamma_up*3e8/2-offset_L)/2;
velo = (beat_up+beat_down)/2*1.55e-6/2;
%% xy
 X_deg = (X_down-0.01 )/((max(X_down)-min(X_down))/2)*11;
%  Y_deg = (Y_down )/((max(X_down)-min(X_down))/2)*15;
Y_deg = (Y_down-0.01)/abs(min(Y_down))*11-4;
 figure   
% scatter3(-X_deg(1:2:end),velo(1:2:end),Y_deg(1:2:end),10,velo(1:2:end), 'filled')
% scatter3(-X_deg,location,Y_deg,5,location, 'filled')
X_scale = sin(-X_deg/180*pi).*(location).*cos(Y_deg/180*pi);
Y_scale = sin(Y_deg/180*pi).*(location);
Z_scale = cos(X_deg/180*pi).*(location).*cos(Y_deg/180*pi);
% scatter3(-X_deg,location,Y_deg,20,velo, 'filled')

figure
scatter3(X_scale ,Z_scale,Y_scale ,10,location, 'filled')
xlabel('x(m)')
ylabel('range')
zlabel('y(m)')
 ylim([0 10])
%% 

    figure
    plot(velo)
hold on
plot(X_deg/15)

figure
scatter(X_scale,Y_scale,10,beat_down,'filled','s')


% %  X_loc = tan(X_rad)*3;
% %  Y_loc = tan(Y_rad)*3;
% 
% % beat((k-1)*n(2)+1:k*n(2)) = F(peakloc)/1e6;
% % v = beat*1e6*1.55e-6/2;
% %  end
% figure
% scatter(X_loc,Y_loc,30,location,'filled','s')
% % scatter(X_deg(start_frame:end_frame),Y_deg(start_frame:end_frame),200,v(start_frame:end_frame),'filled','s')
% axis square
% set(gca,'FontSize',30);
% xlabel('x (m)')
% ylabel('y (m)')
% % ylim ([0 1.44])
% xlim([min(X_loc) max(X_loc)])
% %  
% % xlim([-5 5])
% % ylim([-5 4.3])
%% shift
%     delay = 148;
%     X_loc_shift = circshift(X_loc,-delay);
%     Y_loc_shift = circshift(Y_loc,-delay);
%     X_rad_shift = circshift(X_rad,-delay);
%     Y_rad_shift = circshift(Y_rad,-delay);
%     figure
%     scatter(X_rad_shift(1:end-delay)*180/pi,Y_rad_shift(1:end-delay)*180/pi,50,location(1:end-delay),'filled','s')
%     % scatter(X_deg(start_frame:end_frame),Y_deg(start_frame:end_frame),200,v(start_frame:end_frame),'filled','s')
%     axis square
%     set(gca,'FontSize',30);
%     xlabel('x (deg)')
%     ylabel('y (deg)')
%     ylim ([0 30])
%     xlim ([-15 15])
% %     xlim([min(X_loc) max(X_loc)])
%     
% %% Delay  
%    figure
%    plot(t,location);
%    hold on
%    plot(t,velo);
%   p =  plot(t,X_rad);
%   p.LineWidth = 2;
%       
%    p = plot(t,X_rad_shift,'--');
%    p.LineWidth = 2;
%    xlabel('Time(ms)')
%    set(gca,'FontSize',15);
%    legend('Range(m)','Velocity(m/s)','XFOV(rad)','Shifted XFOV(rad)');

   %% 4D

