% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% author: @xgao
% Velodyne Lidar

clc; close all; clear;

%% Load data

load 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12142021\SampleDataforDSPpipelineVerification_49Drive.mat'
% load 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12142021\SampleDataforDSPpipelineVerification_LinearDrive'


% Integrate drive signal by subinterval to get chirp vs. time
driveSig = T; 
N = length(driveSig);
DriveSigIntgral = zeros(1,N);
DriveSigIntgral(1) = 2*pi*driveSig(1);
for i = 2:N
    DriveSigIntgral(i) = DriveSigIntgral(i-1) + 2*pi*(driveSig(i)+driveSig(i-1))/2/Fs;
end


t = 0:1/Fs:(length(T)-1)/Fs; 
figure
subplot(211)
plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
subplot(212)
plot(t*1e6, DriveSigIntgral); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive chirp')

[minVal, tLaser] = min(DriveSigIntgral);

figure
subplot(221)
plot(t*1e6, I_SDL); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel I)')
subplot(222); spectrogram(I_SDL, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

subplot(223)
plot(t*1e6, Q_SDL); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel Q)')
subplot(224); spectrogram(Q_SDL, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.46e9;             % bandwidth 1GHz
fs = 1e9;               % sampling rate, choice of range 1-3GHz
rangeMax = 100;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
reflIdx = 1.5;          % Reflective index of fiber

% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = 0.1*tUpChirp;         % up-chirp head non-linear portion
t3p = 0.1*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = 0.1*tDownChirp;       % down-chirp head non-linear portion
t3n = 0.1*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% FFT parameter
fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K

disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])

%% Perform segmented FFT, calculate range and velocity
sigMx = I_SDL + 1i*Q_SDL;
sigMxSeg = sigMx(tLaser:end);
tSeg = t(tLaser:end);
numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/Fs;

% converting delay time to samples
t0pSps = round(t0p*fs);
t1pSps = round(t1p*fs);
t2pSps = round(t2p*fs); % fft up-chirp
t3pSps = round(t3p*fs);
t0nSps = round(t0n*fs);
t1nSps = round(t1n*fs);
t2nSps = round(t2n*fs); % fft down-chirp
t3nSps = round(t3n*fs);
tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

mkrFFTupS = t0pSps+t1pSps+1;
mkrFFTupE = mkrFFTupS+t2pSps;
mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
mkrFFTdnE = mkrFFTdnS+t2nSps;

delaySpsStartup = t0pSps+t1pSps;
delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

numSpsTotal = length(sigMxSeg);
numSpsFFT = fftSize;
% fp = [];
% fn = [];
% rangeEst = [];
% vEst = [];


% fftBlkUpChirp = [];
% fftBlkDnChirp = [];
% fftBlkUpChirpIdx = [];
% fftBlkDnChirpIdx = [];
cycleIdx = 1:tSpsFullCycle:numSpsTotal;
cycleLen = length(cycleIdx)-1;
% Verification plot - Highlight delayed and valid FFT regions for verification
figure(4)
subplot(211)
plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
subplot(212)
plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')


fp = zeros(1,cycleLen); % pre-allocate space, speeds things up
fn = zeros(1,cycleLen);
fpMag = zeros(1,cycleLen);
fnMag = zeros(1,cycleLen);
rangeEst = zeros(1,cycleLen); % pre-allocate space, speeds things up
vEst = zeros(1,cycleLen); 

for i = 1:length(cycleIdx)-1
    fftBlkUpChirp = zeros(1,t2pSps); % pre-allocate space for fft chunks (total len = 3333)
    fftBlkDnChirp = zeros(1,t2nSps);
    fftBlkUpChirpIdx = zeros(1,t2pSps);
    fftBlkDnChirpIdx = zeros(1,t2nSps); 
    RunningCtrUp = 1;
    RunningCtrDn = 1;
    sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
    for j = 1:length(sigMx_subSet)
        if j >= mkrFFTupS && j < mkrFFTupE
%             fftBlkUpChirp = [fftBlkUpChirp sigMx_subSet(j)];
%             fftBlkUpChirpIdx = [fftBlkUpChirpIdx cycleIdx(i)+j-1];
            fftBlkUpChirp(RunningCtrUp) = sigMx_subSet(j);
            fftBlkUpChirpIdx(RunningCtrUp) = cycleIdx(i)+j-1;
            RunningCtrUp = RunningCtrUp + 1;
        elseif j >= mkrFFTdnS && j < mkrFFTdnE
%             fftBlkDnChirp = [fftBlkDnChirp sigMx_subSet(j)];
%             fftBlkDnChirpIdx = [fftBlkDnChirpIdx cycleIdx(i)+j-1];
            fftBlkDnChirp(RunningCtrDn) = sigMx_subSet(j);
            fftBlkDnChirpIdx(RunningCtrDn) = cycleIdx(i)+j-1;        
            RunningCtrDn = RunningCtrDn + 1;
        end
    end
        figure(4)
        subplot(211)
        plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
        plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, real(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
        legend('Mixed signal - I', 'FFT region')
        subplot(212)
        plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, imag(fftBlkUpChirp),'color',[0.85,0.33,0.10]);
        plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, imag(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
        legend('Mixed signal - Q', 'FFT region')        
        
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));

        
        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fs;                 % convert in frequency
        figure(5)
%         subplot(floor(numChirpCyc),1,i)
        plot(f, abs(yFFTupChirp)); hold on; plot(f,abs(yFFTdnChirp));
        xlabel('Frequency (Hz)'); ylabel('Magnitude')
        
        [fpMag, fpIdx] = max(abs(yFFTupChirp));
        [fnMag, fnIdx] = max(abs(yFFTdnChirp));
        fpCur = f(fpIdx);
        fnCur = f(fnIdx);
%         subplot(floor(numChirpCyc),1,i)
        plot(fpCur, fpMag,'g*'); text(fpCur, fpMag,['fp = ', num2str(fpCur/1e6),'MHz'])
        plot(fnCur, fnMag,'g^'); text(fnCur*1.2, fnMag,['fn = ', num2str(fnCur/1e6), 'MHz'])
%         legend('Up-chirp FFT', 'Down-chirp FFT','Up-chirp peak', 'Down-chirp peak')
        rangeCur = (fpCur - fnCur)*c/4/alpha_up / reflIdx * 2;
        vCur = -(fpCur + fnCur)*lambda/4;
        
        % Logging estimation results
%         fp = [fp fpCur];
%         fn = [fn fnCur];
        fp(i) = f(fpIdx);
        fn(i) = f(fnIdx);
        fpMag(i) = fpMagCur;
        fnMag(i) = fnMagCur;
%         rangeEst = [rangeEst rangeCur];
%         vEst = [vEst vCur];
        rangeEst(i) = rangeCur;
        vEst(i) = vCur;

        fftBlkUpChirp = [];
        fftBlkDnChirp = [];
        fftBlkUpChirpIdx = [];
        fftBlkDnChirpIdx = [];
end
disp('--------Estimation Results--------')
disp(['Estimated range is ', num2str(rangeEst), 'm'])
disp(['Estimated velocity is ', num2str(vEst), 'm/s'])



rangeRef = 1*reflIdx; 
bwEstUpChirp = abs(fp)/rangeRef*c*tUpChirp; 
disp(['Estimated (conv-bk using upChirp fp) BW is ', num2str(abs(bwEstUpChirp/1e9)), 'GHz'])

bwEstDnChirp = abs(fn)/rangeRef*c*tUpChirp; 
disp(['Estimated (conv-bk using downChirp fn) BW is ', num2str(abs(bwEstDnChirp/1e9)), 'GHz'])
