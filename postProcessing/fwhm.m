% @xgao
% Velodyne Lidar
% Created 12/29/2021

% find full width half maximum on the highest peak using MATLAB inbuilt findpeaks function

function [width] = fwhm(sig, fftSize, Fs, do_plot);

if do_plot
    figure
    findpeaks(sig,'Annotate','extents','WidthReference','halfheight')
end

[pks,locs,w,p]  = findpeaks(sig);
% findpeaks returns 
% 1. a vector with the local maxima (peaks) of the input signal vector, sig
% 2. indices at which the peaks occur
% 3. widths of the peaks as the vector w 
% 4. prominences of the peaks as the vector p.
% Note: A local peak is a data sample that is either larger than its two neighboring samples or is equal to Inf.

[pkVal,pkIdx] = max(pks); % locates the highest peak value and index among found peaks

% convert width from sample into frequency
width = w(pkIdx)/fftSize * Fs; % w(pkIdx) finds half maximum width, unit in sample points (max = fftSize)