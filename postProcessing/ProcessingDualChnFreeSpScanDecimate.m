% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% author: @xgao
% Velodyne Lidar

% External function Red Blue Colormap: redblue.m
% version 1.0.0.0 (1.47 KB) by Adam Auton

clc; close all; clear;

%% Load data
rangeRef = [4.8 2.5];
n = input('Enter a data set number: ');
do_FFTplot = input('Do FFT plot (longer delay)? 1 for Y, 0 for N: ');
% 
switch n
    case 1
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12142021\Scan_4point8m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12142021\Scan_4point8m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12142021\Scan_4point8m_Trigger.mat';
        opticalPathDly = 6;
    case 2
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12222021\XYScan_2point5m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12222021\XYScan_2point5m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12222021\XYScan_2point5m_Trigger.mat';
%         do_FFTplot = 0;
        scanPos_recorded = 1;
        filenameX = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12222021\XYScan_2point5m_X.mat';
        filenameY = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12222021\XYScan_2point5m_Y.mat';
        opticalPathDly = 6;
    otherwise
        disp('Wrong entry, please choose between 1 and 2')
end
disp(['Reference range is ', num2str(rangeRef(n)), 'm'])
 
load (filenameI)
load (filenameQ)
load (filenameTrigger)

%% Initialize data
% Integrate drive signal by subinterval to get chirp vs. time
Fs = 1e9;
driveSig = Ch4s(1:25e6); % processing first 1M data
N = length(driveSig);
DriveSigIntgral = zeros(1,N);
DriveSigIntgral(1) = 2*pi*driveSig(1);
for i = 2:N
    DriveSigIntgral(i) = DriveSigIntgral(i-1) + 2*pi*(driveSig(i)+driveSig(i-1))/2/Fs;
end


t = 0:1/Fs:(N-1)/Fs; 
figure
subplot(211)
plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
subplot(212)
plot(t*1e6, DriveSigIntgral); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive chirp')

[minVal tLaser] = min(DriveSigIntgral);
if tLaser > N/2
   [minVal tLaser] = min(-DriveSigIntgral);
end
% t = 0:1/Fs:(N/10-1)/Fs;  % decimate by 10
% I_Raw = Ch1s(1:N/10);
% Q_Raw = Ch2s(1:N/10);

t = 0:1/Fs:(N-1)/Fs;  % processing a portion 
I_Raw = Ch1s(1:N);
Q_Raw = Ch2s(1:N);

figure
subplot(221)
plot(t*1e6, I_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel I)')
subplot(222); spectrogram(I_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

subplot(223)
plot(t*1e6, Q_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel Q)')
subplot(224); spectrogram(Q_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')


% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.5e9;             % bandwidth 1GHz
fs = 1e9;               % sampling rate, choice of range 1-3GHz
fsDecimate = fs/2;
rangeMax = 100;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
sampleCropRate = 0.1;       % portion cropped at head and tail of chirp
thresholdMag =  4;
% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = sampleCropRate*tUpChirp;         % up-chirp head non-linear portion
t3p = sampleCropRate*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = sampleCropRate*tDownChirp;       % down-chirp head non-linear portion
t3n = sampleCropRate*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% Scanner position data loading and decimation
if scanPos_recorded == 1
    load (filenameX);
    load (filenameY);
    rawPosX = X(1:N);
    rawPosY = Y(1:N);
    
    samplesOffset = 1.5e-3 * fs;
    rawPosX = circshift(rawPosX,-samplesOffset);
    rawPosY = circshift(rawPosY,-samplesOffset);
    posXdecimated = rawPosX(tLaser:(tUpChirp + tDownChirp)*fs:end);
    posYdecimated = rawPosY(tLaser:(tUpChirp + tDownChirp)*fs:end);
    figure
    subplot(231); plot(rawPosX); xlabel('samples'); ylabel('X position');title('raw position X');
    subplot(232); plot(rawPosY); xlabel('samples'); ylabel('Y position');title('raw position Y');
    subplot(233); plot(rawPosX, rawPosY,'o');xlabel('x'); ylabel('y'); title('scanning pattern')
    hold on; plot(rawPosX(1), rawPosY(1), 'ro')
    subplot(234); plot(posXdecimated); xlabel('samples'); ylabel('X position');title('decimated position X');
    subplot(235); plot(posYdecimated); xlabel('samples'); ylabel('Y position');title('decimated position Y');
    subplot(236); plot(posXdecimated, posYdecimated,'o');xlabel('x'); ylabel('y'); title('scanning pattern')
    hold on; plot(posXdecimated(1), posYdecimated(1), 'ro')
end

% FFT parameter
% fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K
fftSize = 2048;
disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])
disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])
%% Perform segmented FFT, calculate range and velocity
sigMx = I_Raw + 1i*Q_Raw;
sigMxSeg = sigMx(tLaser:end);
tSeg = t(tLaser:end);
numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/Fs;

% converting delay time to samples
t0pSps = round(t0p*fs);
t1pSps = round(t1p*fs);
t2pSps = round(t2p*fs); % fft up-chirp
t3pSps = round(t3p*fs);
t0nSps = round(t0n*fs);
t1nSps = round(t1n*fs);
t2nSps = round(t2n*fs); % fft down-chirp
t3nSps = round(t3n*fs);
tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

mkrFFTupS = t0pSps+t1pSps+1;
mkrFFTupE = mkrFFTupS+t2pSps;
mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
mkrFFTdnE = mkrFFTdnS+t2nSps;

delaySpsStartup = t0pSps+t1pSps;
delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

numSpsTotal = length(sigMxSeg);
numSpsFFT = fftSize;
fp = [];
fn = [];
fpMag = [];
fnMag = [];
fftBlkUpChirpFull = [];
fftBlkDnChirpFull = [];
fftBlkUpChirpIdxFull = [];
fftBlkDnChirpIdxFull = [];
fftBlkUpChirp = [];
fftBlkDnChirp = [];
fftBlkUpChirpIdx = [];
fftBlkDnChirpIdx = [];
snrUp = [];
snrDn = [];
snrUpdB = [];
snrDndB = [];
cycleIdx = 1:tSpsFullCycle:numSpsTotal;


% Verification plot - Highlight delayed and valid FFT regions for verification
% figure(4)
% subplot(211)
% plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
% subplot(212)
% plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')

for i = 1:length(cycleIdx)-1

    sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
    for j = 1:length(sigMx_subSet)
        if j >= mkrFFTupS && j < mkrFFTupE
            fftBlkUpChirpFull = [fftBlkUpChirpFull sigMx_subSet(j)];
            fftBlkUpChirpIdxFull = [fftBlkUpChirpIdxFull cycleIdx(i)+j-1];
        elseif j >= mkrFFTdnS && j < mkrFFTdnE
            fftBlkDnChirpFull = [fftBlkDnChirpFull sigMx_subSet(j)];
            fftBlkDnChirpIdxFull = [fftBlkDnChirpIdxFull cycleIdx(i)+j-1];
        end
    end
%         figure(4)
%         subplot(211)
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, real(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - I', 'FFT region')
%         subplot(212)
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, imag(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, imag(fftBlkUpChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - Q', 'FFT region')        
        decimateRate = round(length(fftBlkUpChirpFull)/2048);
        fftBlkUpChirp = fftBlkUpChirpFull(1:decimateRate:end);
        fftBlkDnChirp = fftBlkDnChirpFull(1:decimateRate:end);
        fftBlkUpChirpIdx = fftBlkUpChirpIdxFull(1:decimateRate:end);
        fftBlkDnChirpIdx = fftBlkDnChirpIdxFull(1:decimateRate:end);
                
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));
        
        % Masking
        maskFreqsUpChirp = [-8e6 4e6]; % masking frequency upper bound
        maskFreqsDnChirp = [-4e6 8e6];
        maskIdxUpChirp = round(maskFreqsUpChirp/fsDecimate * fftSize);
        maskIdxDnChirp = round(maskFreqsDnChirp/fsDecimate * fftSize);
        yFFTupChirp(fftSize/2+maskIdxUpChirp(1) : fftSize/2+maskIdxUpChirp(2)) = 0;
        yFFTdnChirp(fftSize/2+maskIdxDnChirp(1): fftSize/2+maskIdxDnChirp(2)) = 0;
        
        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fsDecimate;                 % convert in frequency

        % Peak detection
        [fpMagCur, fpIdx] = max(abs(yFFTupChirp));
        [fnMagCur, fnIdx] = max(abs(yFFTdnChirp));
        
        % SNR calculation
        noiseBandIdx = round([-200e6 -50e6 50e6 200e6]/fsDecimate*fftSize); 
        snrUpCur = fpMagCur/rms(abs(yFFTupChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));
        snrDnCur = fnMagCur/rms(abs(yFFTdnChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));
        snrUp = [snrUp snrUpCur];
        snrDn = [snrDn snrDnCur];
        snrUpdB = [snrUpdB 10*log10(snrUpCur)];
        snrDndB = [snrDndB 10*log10(snrDnCur)];

         % Logging estimation results
        fp = [fp f(fpIdx)];
        fn = [fn f(fnIdx)];
        fpMag = [fpMag fpMagCur];
        fnMag = [fnMag fnMagCur];
        
        % Investigation on Masking         
        if do_FFTplot == 1
            figure(5) % original spectrum
            plot(f/1e6, abs(yFFTupChirp)); hold on; plot(f/1e6,abs(yFFTdnChirp)); 
            xlabel('Frequency (MHz)'); ylabel('Magnitude')
            
            figure(6) % stack FFTs 
            subplot(211)
            plot(f, abs(yFFTupChirp)); hold on;
            xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Up-chirp FFT of all cycles')
            xlim([-1e8 1e8])
            subplot(212)
            plot(f,abs(yFFTdnChirp)); hold on;
            xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Down-chirp FFT of all cycles')
            xlim([-1e8 1e8])
        end
  
        fftBlkUpChirpFull = [];
        fftBlkDnChirpFull = [];
        fftBlkUpChirpIdxFull = [];
        fftBlkDnChirpIdxFull = [];
end


% Histogram of SNR
figure
subplot(311)
plot(snrUpdB);
hold on;
plot(snrDndB);
xlabel('up/down chirp cycles')
ylabel('SNR (dB)')
legend('upchirp SNR', 'downchirp SNR')

disp(['Mean, Median, std SNR of up-chirp is ', num2str(mean(snrUpdB)), ' ', num2str(median(snrUpdB)), ' ', num2str(std(snrUpdB)), ' dB'])
disp(['Mean, Median, std SNR of down-chirp is ', num2str(mean(snrDndB)), ' ', num2str(median(snrDndB)), ' ', num2str(std(snrDndB)), ' dB'])
subplot(312);histogram(snrUpdB,'EdgeColor',[1 1 1])
xlabel('SNR (dB)');title('Upchirp FFT SNR distribution (no thresholding)')

subplot(313); histogram(snrDndB,'EdgeColor',[1 1 1])
xlabel('SNR (dB)');title('Downchirp FFT SNR distribution (no thresholding)')

%% Calculate SNR, range & velocity based on thresholding results
rangeEst = [];
vEst = [];
fpOut = [];
fnOut = [];

for i = 1:length(fp)
    % Thresholding
    fpCur = fp(i);
    fnCur = fn(i);
    fpMagCur = fpMag(i);
    fnMagCur = fnMag(i);
    
    if fpMagCur > thresholdMag && fnMagCur > thresholdMag
        fpOut = [fpOut fpCur];
        fnOut = [fnOut fnCur];
        % Range and Velocity calculation
        rangeCur = ((fpCur - fnCur)*c/2/alpha_up - opticalPathDly)/2; % considering optical length differences
        vCur = -(fpCur + fnCur)*lambda/4;
        rangeEst = [rangeEst rangeCur];
        vEst = [vEst vCur];

    else
        fpOut = [fpOut 0];
        fnOut = [fnOut 0];
        rangeEst = [rangeEst 0];
        vEst = [vEst 0];
    end

end

disp('--------Estimation Results--------')
disp(['Estimated range is ', num2str(rangeEst), 'm'])
disp(['Estimated velocity is ', num2str(vEst), 'm/s'])
disp(['Mean range is ', num2str(mean(rangeEst)), 'm'])
disp(['Mean velocity is ', num2str(mean(vEst)), 'm/s'])
disp(['Mean, Median, std fp is ', num2str(mean(fpOut)/1e6), ' ', num2str(median(fpOut)/1e6), ' ', num2str(std(fpOut)/1e6), ' MHz'])
disp(['Mean, Median, std fn is ', num2str(mean(fnOut)/1e6), ' ', num2str(median(fnOut)/1e6), ' ', num2str(std(fnOut)/1e6),' MHz'])

%% Distribution plots

% 1. Histogram of beat tone
edgesUpChirpBeatTone = [-15:0.5:5]; % 10MHz wide bins
edgesDnChirpBeatTone = [-5:0.5:15]; % 10MHz wide bins
% figure
% subplot(211);histogram(fp/1e6,edgesUpChirpBeatTone,'EdgeColor',[1 1 1])
% xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution')
% subplot(212); histogram(fn/1e6,edgesDnChirpBeatTone,'EdgeColor',[1 1 1])
% xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution')

figure
subplot(221);histogram(fp/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution')
subplot(223); histogram(fn/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution')
subplot(222);histogram(fpOut/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution with thresholding')
subplot(224); histogram(fnOut/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution with thresholding')

% 
% rangeRef = rangeRef(n); 
% bwEstUpChirp = abs(fp)/rangeRef*c*tUpChirp; 
% disp(['Estimated (conv-bk using upChirp fp) BW is ', num2str(abs(bwEstUpChirp/1e9)), 'GHz'])
% 
% bwEstDnChirp = abs(fn)/rangeRef*c*tUpChirp; 
% disp(['Estimated (conv-bk using downChirp fn) BW is ', num2str(abs(bwEstDnChirp/1e9)), 'GHz'])

%% Range and velocity estimation results
figure
subplot(211)
plot(rangeEst); xlabel('cycle#'); ylabel('range(m)');title('Range Estimation')
ylim([0 10])
subplot(212)
plot(vEst,'r'); xlabel('cycle#'); ylabel('velocity(m/s)');title('Velocity Estimation')
ylim([-2 2])

%% Scatter plot
% 1. 2D scatter plot
L = min(length(posXdecimated), length(rangeEst));
figure
subplot(211)
scatter(posXdecimated(1:L),posYdecimated(1:L),10,rangeEst(1:L),'filled','s')
xlabel('X'); ylabel('Y'); title('Range map');colorbar
subplot(212)
scatter(posXdecimated(1:L),posYdecimated(1:L),10,vEst(1:L),'filled','s')
xlabel('X'); ylabel('Y'); title('Velocity map');colorbar

% 2. 3D scatter plot
markerSize = 50;
scatter_x = posXdecimated(1:L);
scatter_y = posYdecimated(1:L);
scatter_z1 = rangeEst(1:L);
scatter_z2 = vEst(1:L);

figure
s1 = scatter3(scatter_x,scatter_z1,scatter_y,markerSize,scatter_z2, 'filled'); % use velocity as color scale to show dots of range 3D map
title('Range & velocity estimation')
xlabel('X scan'); ylabel('Range (m)'); zlabel('Y scan')
colorbar
colormap(redblue(20))
view(20, 40)

