% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% author: @xgao
% Velodyne Lidar

% External function Red Blue Colormap: redblue.m
% version 1.0.0.0 (1.47 KB) by Adam Auton

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

clc; close all; clear;
tic
%% Load data
rangeRef = [3.0 3.0 3.0 3.0]; % reference range of the major target (placeholder, may remove)
n = input('Enter a data set number: ');
segTotal = input('Enter how many segments to run: ');
do_masking = input('Do masking? Y(1)/N(0): ');
do_infoPlot = input('Do information plot? Y(1)/N(0): ');
do_EstResultDisplay = input('Do estimation result display? Y(1)/N(0): ');
do_FFTplot = input('Do FFT plot with longer wait time? Y(1)/N(0): ');
do_saveOutputData = input('Save output data? Y(1)/N(0): ');
%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.5e9;             % bandwidth 1GHz
fs = 1e9;               % sampling rate, choice of range 1-3GHz
rangeMax = 100;         % Maximum target range 100m  
% rangeMax = 67;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
sampleCropRate = 0.1;       % portion cropped at head and tail of chirp
% sampleCropRate = 0.0458;       % portion cropped at head and tail of chirp


% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = sampleCropRate*tUpChirp;         % up-chirp head non-linear portion
t3p = sampleCropRate*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = sampleCropRate*tDownChirp;       % down-chirp head non-linear portion
t3n = sampleCropRate*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K
disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])

%% Sweep segment # to run processing script
outputPtMax = segTotal * 25e6/((tUpChirp + tDownChirp)*fs); 
outputPtIdx_CurSegStart = 0;
outputClusterFilename  = [];

outputX = zeros(1,outputPtMax);
outputY = zeros(1,outputPtMax);
outputRange = zeros(1,outputPtMax);
outputVelocity = zeros(1,outputPtMax);
outputX_meter = zeros(1,outputPtMax);
outputY_meter = zeros(1,outputPtMax);
outputRange_meter = zeros(1,outputPtMax);

for segNum = 1:segTotal
%% Load data
    switch n
        case 1
            filenameI = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01112022\XYScan_I_',num2str(segNum),'.mat']);
            filenameQ = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01112022\XYScan_Q_',num2str(segNum),'.mat']);
            filenameTrigger = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01112022\XYScan_Trigger_',num2str(segNum),'.mat']);
            filenameX = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01112022\XYScan_X_',num2str(segNum),'.mat']);
            filenameY = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01112022\XYScan_Y_',num2str(segNum),'.mat']);
            calibrationAuto = 0;
            calibrationSpPt = [367968;299343;278519;410338;301524;311325];
            thresholdMag =  8;
            opticalPathDly = 6;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-8e6 8e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-8e6 8e6];
        case 2
            filenameI = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_I_',num2str(segNum),'.mat']);
            filenameQ = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_Q_',num2str(segNum),'.mat']);
            filenameTrigger = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_Trigger_',num2str(segNum),'.mat']);
            filenameX = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_X_',num2str(segNum),'.mat']);
            filenameY = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_Y_',num2str(segNum),'.mat']);
            calibrationAuto = 1;
            calibrationX2 = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\XYScan_X2_',num2str(segNum),'.mat']);
            thresholdMag =  6;
            opticalPathDly = 6;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-8e6 8e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-8e6 8e6];
        case 3
            filenameI = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_I_',num2str(segNum),'.mat']);
            filenameQ = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_Q_',num2str(segNum),'.mat']);
            filenameTrigger = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_Trigger_',num2str(segNum),'.mat']);
            filenameX = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_X_',num2str(segNum),'.mat']);
            filenameY = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_Y_',num2str(segNum),'.mat']);
            calibrationAuto = 1;
            calibrationX2 = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01132022\XYScan_X2_',num2str(segNum),'.mat']);
            thresholdMag =  5;
            opticalPathDly = 6;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-8e6 8e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-8e6 8e6];
        case 4
            filenameI = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_I_',num2str(segNum),'.mat']);
            filenameQ = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_Q_',num2str(segNum),'.mat']);
            filenameTrigger = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_Trigger_',num2str(segNum),'.mat']);
            filenameX = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_X_',num2str(segNum),'.mat']);
            filenameY = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_Y_',num2str(segNum),'.mat']);
            calibrationAuto = 1;
            calibrationX2 = (['C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01142022\XYScan_X2_',num2str(segNum),'.mat']);         
            thresholdMag =  6;
            opticalPathDly = 3;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-4e6 4e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-4e6 4e6];

        otherwise
            disp('Wrong entry, please enter 1-4')
    end
    disp(['Reference range is ', num2str(rangeRef(n)), 'm'])

    % Raw data loading
    load (filenameI)
    load (filenameQ)
    load (filenameTrigger)

    % Scanner position data loading and decimation
    load (filenameX);
    load (filenameY);

    %% Load calibration dataset, either pre-calculated or runtime cal

    if calibrationAuto == 1 % auto calibration
        load(calibrationX2);
        samplesOffsetScope = finddelay(Ch3, Ch8); % Ch3 = X2, Ch8 = X. Ch3 is reference X signal, this is to determine delay on X from X2
        samplesOffsetScanner = 0.15e-3 * fs;
%         if sampleOffset < 0
%             samplesOffset = finddelay(Ch3, Ch8); 
%         else
%         end
    else % manual calibration
        samplesOffsetScope = calibrationSpPt(segNum); % pre-calculated delay stored in array calibrationSpPt

    end
    clear Ch3;
    %% Initialize data
    % I and Q signal with tail portion chopped, matching delay fix
    I_Raw = Ch1(1:end-samplesOffsetScope-samplesOffsetScanner);
    Q_Raw = Ch2(1:end-samplesOffsetScope-samplesOffsetScanner);
    clear Ch1 Ch2;
    % Integrate drive signal by subinterval to get chirp vs. time
    driveSig = Ch4(1:end-samplesOffsetScope-samplesOffsetScanner); % Trigger signal with tail portion chopped, matching delay fix
    clear Ch4;
    N = length(driveSig);
    DriveSigIntgral = zeros(1,N);
    DriveSigIntgral(1) = 2*pi*driveSig(1);
    for i = 2:N
        DriveSigIntgral(i) = DriveSigIntgral(i-1) + 2*pi*(driveSig(i)+driveSig(i-1))/2/fs;
    end
    t = 0:1/fs:(N-1)/fs; 

    [minVal, tLaser] = min(DriveSigIntgral);
    if tLaser > N/2
       [minVal, tLaser] = min(-DriveSigIntgral);
    end
    if do_infoPlot
        figure
        subplot(211)
        plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
        subplot(212)
        plot(t*1e6, DriveSigIntgral); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive chirp')
    end
    clear driveSig DriveSigIntgral;
    
% X and Y signal with head portion chopped, to fix delay
    rawPosX = Ch8(samplesOffsetScope+samplesOffsetScanner+1:end); % Xscan is Ch8s
    rawPosY = Ch7(samplesOffsetScope+samplesOffsetScanner+1:end); % Yscan is Ch7s
    clear Ch7 Ch8;
    posXdecimated = rawPosX(tLaser+500:round((tUpChirp + tDownChirp)*fs):end);
    posYdecimated = rawPosY(tLaser+500:round((tUpChirp + tDownChirp)*fs):end);
    if do_infoPlot
        figure
        subplot(231); plot(rawPosX); xlabel('samples'); ylabel('X position');title('raw position X');
        subplot(232); plot(rawPosY); xlabel('samples'); ylabel('Y position');title('raw position Y');
        subplot(233); plot(rawPosX, rawPosY,'o');xlabel('x'); ylabel('y'); title('scanning pattern')
        hold on; plot(rawPosX(1), rawPosY(1), 'ro')
        subplot(234); plot(posXdecimated); xlabel('samples'); ylabel('X position');title('decimated position X');
        subplot(235); plot(posYdecimated); xlabel('samples'); ylabel('Y position');title('decimated position Y');
        subplot(236); plot(posXdecimated, posYdecimated,'o');xlabel('x'); ylabel('y'); title('scanning pattern')
        hold on; plot(posXdecimated(1), posYdecimated(1), 'ro')
    end

    % figure
    % subplot(221)
    % plot(t*1e6, I_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Raw Data Channel I')
    % subplot(222); spectrogram(I_Raw, 2048, 2000, 2048, Fs, 'yaxis')
    % colorbar('off')
    % 
    % subplot(223)
    % plot(t*1e6, Q_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Raw Data Channel Q')
    % subplot(224); spectrogram(Q_Raw, 2048, 2000, 2048, Fs, 'yaxis')
    % colorbar('off')

    %% FFT 
    % Perform segmented FFT, calculate range and velocity
    sigMx = I_Raw + 1i*Q_Raw;
    sigMxSeg = sigMx(tLaser:end);
    tSeg = t(tLaser:end);
    numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/fs;

    % converting delay time to samples
    t0pSps = round(t0p*fs);
    t1pSps = round(t1p*fs);
    t2pSps = round(t2p*fs); % fft up-chirp
    t3pSps = round(t3p*fs);
    t0nSps = round(t0n*fs);
    t1nSps = round(t1n*fs);
    t2nSps = round(t2n*fs); % fft down-chirp
    t3nSps = round(t3n*fs);
    tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

    mkrFFTupS = t0pSps+t1pSps+1;
    mkrFFTupE = mkrFFTupS+t2pSps;
    mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
    mkrFFTdnE = mkrFFTdnS+t2nSps;

    delaySpsStartup = t0pSps+t1pSps;
    delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
    delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

    numSpsTotal = length(sigMxSeg);
    numSpsFFT = fftSize;

    % Define how many cycles (10us chirps) within each segment
    cycleIdx = 1:tSpsFullCycle:numSpsTotal;
    cycleLen = length(cycleIdx)-1;


%     Verification plot - Highlight delayed and valid FFT regions for verification
%     figure(4)
%     subplot(211)
%     plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
% %     plot(t*1e6, driveSig,'c'); xlabel('time(us)'); ylabel('voltage(v)');
%     xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
%     subplot(212)
%     plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
% %     plot(t*1e6, driveSig,'c'); xlabel('time(us)'); ylabel('voltage(v)'); 
%     xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')
    fp = zeros(1,cycleLen); % pre-allocate space, speeds things up
    fn = zeros(1,cycleLen);
    fpMag = zeros(1,cycleLen);
    fnMag = zeros(1,cycleLen);
    NoiseUpChirpMag = zeros(1,cycleLen);
    NoiseDnChirpMag = zeros(1,cycleLen);
    %% Sliced FFT
    for i = 1:length(cycleIdx)-1
    % Initialize variables
        
        fftBlkUpChirp = zeros(1,t2pSps); % pre-allocate space for fft chunks (total len = 3333)
        fftBlkDnChirp = zeros(1,t2nSps);
        fftBlkUpChirpIdx = zeros(1,t2pSps);
        fftBlkDnChirpIdx = zeros(1,t2nSps); 
        RunningCtrUp = 1;
        RunningCtrDn = 1;
        sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
        for j = 1:length(sigMx_subSet)
            if j >= mkrFFTupS && j < mkrFFTupE
                fftBlkUpChirp(RunningCtrUp) = sigMx_subSet(j);
                fftBlkUpChirpIdx(RunningCtrUp) = cycleIdx(i)+j-1;
                RunningCtrUp = RunningCtrUp + 1;
            elseif j >= mkrFFTdnS && j < mkrFFTdnE
                fftBlkDnChirp(RunningCtrDn) = sigMx_subSet(j);
                fftBlkDnChirpIdx(RunningCtrDn) = cycleIdx(i)+j-1;
                RunningCtrDn = RunningCtrDn + 1;
            end
        end
%             figure(4)
%             subplot(211)
%             plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
%             plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, real(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%             legend('Mixed signal - I', 'FFT region')
%             subplot(212)
%             plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, imag(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%             plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, imag(fftBlkUpChirp),'color',[0.85,0.33,0.10]);
%             legend('Mixed signal - Q', 'FFT region')        


%       % Default MATLAB fft with zero-padding at the end
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));

        % Masking
        if do_masking
            maskIdxUpChirp = round(maskFreqsUpChirp/fs * fftSize);
            maskIdxDnChirp = round(maskFreqsDnChirp/fs * fftSize);
            yFFTupChirp(fftSize/2+maskIdxUpChirp(1) : fftSize/2+maskIdxUpChirp(2)) = 0;
            yFFTdnChirp(fftSize/2+maskIdxDnChirp(1): fftSize/2+maskIdxDnChirp(2)) = 0;
        else
        end

        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fs;                 % convert in frequency

        % Peak detection
        [fpMagCur, fpIdx] = max(abs(yFFTupChirp));
        [fnMagCur, fnIdx] = max(abs(yFFTdnChirp));

        %% Noise band rms value calculation
        % SNR calculation
        noiseBandIdx = round([-200e6 -50e6 50e6 200e6]/fs*fftSize); 
        rmsNoiseMagUpChirpCur = rms(abs(yFFTupChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));
        rmsNoiseMagDnChirpCur = rms(abs(yFFTdnChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));

        fp(i) = f(fpIdx);
        fn(i) = f(fnIdx);
        fpMag(i) = fpMagCur;
        fnMag(i) = fnMagCur;
        NoiseUpChirpMag(i) = rmsNoiseMagUpChirpCur;
        NoiseDnChirpMag(i) = rmsNoiseMagDnChirpCur;
        % Investigation on Masking         
        if do_FFTplot == 1
            figure(5) % original spectrum
            plot(f/1e6, abs(yFFTupChirp)); hold on; plot(f/1e6,abs(yFFTdnChirp)); 
            xlabel('Frequency (MHz)'); ylabel('Magnitude')

            figure(6) % stack FFTs 
            subplot(211)
            plot(f/1e6, abs(yFFTupChirp)); hold on;
            xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Up-chirp FFT of all cycles')
            xlim([-1e8 1e8])
            subplot(212)
            plot(f/1e6,abs(yFFTdnChirp)); hold on;
            xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Down-chirp FFT of all cycles')
            xlim([-1e8 1e8])
        end
        fftBlkUpChirp = [];
        fftBlkDnChirp = [];
        fftBlkUpChirpIdx = [];
        fftBlkDnChirpIdx = [];
    end

    %% Perform thresholding, then Calculate SNR, Range & velocity based results
    rangeEst = zeros(1,cycleLen); % pre-allocate space, speeds things up
    vEst = zeros(1,cycleLen); 
    fpOut = zeros(1,cycleLen); % output beat tone with threshold applied
    fnOut = zeros(1,cycleLen);
    snrOut = zeros(1,cycleLen);
    snrUp = zeros(1,cycleLen);
    snrDn = zeros(1,cycleLen);
    snrUpdB = zeros(1,cycleLen);
    snrDndB = zeros(1,cycleLen);
    %% Range and Velocity Cal
    for i = 1:length(fp)
        % Thresholding
        fpCur = fp(i);
        fnCur = fn(i);
        fpMagCur = fpMag(i);
        fnMagCur = fnMag(i);
        rmsNoiseMagUpChirpCur = NoiseUpChirpMag(i);
        noiseDnCur = NoiseDnChirpMag(i);
        if fpMagCur > thresholdMag && fnMagCur > thresholdMag
            fpOut(i) = fpCur;
            fnOut(i) = fnCur;
            % SNR calculation
            snrUpCur = fpMagCur/rmsNoiseMagUpChirpCur;
            snrDnCur = fnMagCur/noiseDnCur;

            snrUp(i) = snrUpCur;
            snrDn(i) = snrDnCur;            
            snrUpdB(i) = 10*log10(snrUpCur);
            snrDndB(i) = 10*log10(snrDnCur);
            % Range and Velocity calculation
            rangeCur = ((fpCur - fnCur)*c/2/alpha_up - opticalPathDly)/2; % considering optical length differences
            vCur = -(fpCur + fnCur)*lambda/4;
            rangeEst(i) = rangeCur;
            vEst(i) = vCur;

        else
            fpOut(i) = 0;
            fnOut(i) = 0;
            % SNR calculation
            snrUp(i) = 0;
            snrDn(i) = 0;            
            snrUpdB(i) = 0;
            snrDndB(i) = 0;
            % Range and Velocity calculation
            rangeEst(i) = 0;
            vEst(i) = 0;
        end

    end
    
    if do_EstResultDisplay
        disp('--------Estimation Results--------')
        disp(['Segment # ', num2str(segNum)])
        disp(['Estimated range is ', num2str(rangeEst), 'm'])
        disp(['Estimated velocity is ', num2str(vEst), 'm/s'])
        disp(['Mean range is ', num2str(mean(rangeEst)), 'm'])
        disp(['Mean velocity is ', num2str(mean(vEst)), 'm/s'])
        disp(['Mean, Median, std fp is ', num2str(mean(fpOut)/1e6), ' ', num2str(median(fpOut)/1e6), ' ', num2str(std(fpOut)/1e6), ' MHz'])
        disp(['Mean, Median, std fn is ', num2str(mean(fnOut)/1e6), ' ', num2str(median(fnOut)/1e6), ' ', num2str(std(fnOut)/1e6),' MHz'])
    end
   
    %% Distribution plots

    % Histogram of beat tone
    if do_infoPlot
        figure
        subplot(221);histogram(fp/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution')
        subplot(223); histogram(fn/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution')
        subplot(222);histogram(fpOut/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution with thresholding')
        subplot(224); histogram(fnOut/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution with thresholding')
    end
    %% Range and velocity estimation results
    if do_infoPlot
        figure
        subplot(211)
        plot(rangeEst); xlabel('cycle#'); ylabel('range(m)');title('Range Estimation')
        % ylim([0 10])
        subplot(212)
        plot(vEst,'r'); xlabel('cycle#'); ylabel('velocity(m/s)');title('Velocity Estimation')
        % ylim([-2 2])
    end
    %% Scatter plot
    L = min(length(posXdecimated), length(rangeEst));
    
    % 3D scatter plot
    markerSize = 20;
    % old 
    scatter_x = posXdecimated(1:L)/max(posXdecimated)*15;
    scatter_y = posYdecimated(1:L)/max(posXdecimated)*15;
    % new
%     scatter_x = (posXdecimated(1:L) - mean(posXdecimated(1:L)))/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_y = (posYdecimated(1:L) - mean(posXdecimated(1:L)))/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_x = (posXdecimated(1:L) - 0.01)/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_y = (posYdecimated(1:L) - 0.01)/(abs(min(posXdecimated(1:L)))/2)*15 - 5.5;

    scatter_rng = rangeEst(1:L);
    scatter_v = vEst(1:L);

    scatter_x_meter = sin(scatter_x/180*pi).*(rangeEst(1:L)').*cos(scatter_y/180*pi);
    scatter_y_meter = sin(scatter_y/180*pi).*(rangeEst(1:L)');
    scatter_rng_meter = cos(scatter_x/180*pi).*(rangeEst(1:L)').*cos(scatter_y/180*pi);

    if do_infoPlot
        figure
        sSeg = scatter3(scatter_x,scatter_rng,scatter_y,markerSize,scatter_v, 'filled'); % use velocity as color scale to show dots of range 3D map
        title(['Range & velocity estimation (seg #', num2str(segNum),')'])
        xlabel('X scan'); ylabel('Range (m)'); zlabel('Y scan')
        colorbar
        colormap(redblue(20))
        view(20, 40)
    end

    %% Spectrum plot
    if do_infoPlot
        figure
        subplot(221); plot(fp/1e6, fpMag,'o'); xlabel('Frequency (MHz)'); ylabel('Up-chirp'); title('w/o thresholding')
        subplot(223); plot(fn/1e6, fnMag,'o'); xlabel('Frequency (MHz)'); ylabel('Down-chirp');title('w/o thresholding')
        subplot(222); plot(fpOut/1e6, fpMag,'go'); xlabel('Frequency (MHz)'); title('w/ thresholding');% xlim([-fs/2 fs/2-1]);
        subplot(224); plot(fnOut/1e6, fnMag,'go'); xlabel('Frequency (MHz)'); title('w/ thresholding');% xlim([-fs/2 fs/2-1]);
    end

    % Logging all results
    outputX(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_x';
    outputY(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_y';
    outputRange(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_rng;
    outputVelocity(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_v;

    outputX_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_x_meter';
    outputY_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_y_meter';
    outputRange_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_rng_meter';
    
    % update index start to total length of current segment
    outputPtIdx_CurSegStart = outputPtIdx_CurSegStart + L;

    if do_saveOutputData == 1
        outputClusterFilename =['saveOutput_Seg',num2str(segNum),'.mat'];
        save(outputClusterFilename,'snrUp','snrDn','snrUpdB','snrDndB','fp','fn', ...
            'fpMag','fnMag','fpOut','fnOut','rangeEst','vEst','outputX','outputY',...
            'outputRange','outputVelocity');
    else
    end
end

% removing residual empty space allocated for outputs. 
% outputPtIdx_CurSegStart after 15 segment loops is not at end of valid
% output index
outputX = outputX(1:outputPtIdx_CurSegStart);
outputY = outputY(1:outputPtIdx_CurSegStart);
outputRange = outputRange(1:outputPtIdx_CurSegStart);
outputVelocity = outputVelocity(1:outputPtIdx_CurSegStart);
outputX_meter = outputX_meter(1:outputPtIdx_CurSegStart);
outputY_meter = outputY_meter(1:outputPtIdx_CurSegStart);
outputRange_meter = outputRange_meter(1:outputPtIdx_CurSegStart);


figure
scatter3(-outputX,outputRange,outputY,5,outputVelocity, 'filled'); % use velocity as color scale to show dots of range 3D map
title('Range & velocity estimation')
xlabel('X scan (deg)'); ylabel('Range (m)'); zlabel('Y scan (deg)')
colorbar
colormap(redblue(20))
view(20, 40)
title('Range vs. velocity 4D scatter plot')
ylim([0.1 10])

figure
scatter3(-outputX,outputRange,outputY,5,outputRange, 'filled'); % use range as color scale to show dots of range 3D map
title('Range & velocity estimation')
xlabel('X scan (deg)'); ylabel('Range (m)'); zlabel('Y scan (deg)')
colorbar
% colormap(redblue(20))
view(20, 40)
title('Range 3D scatter plot (deg)')
% caxis([-5 10])
ylim([0.1 10])

%% Convert to meter scale

figure
scatter3(-outputX_meter,outputRange_meter,outputY_meter,10,outputRange_meter, 'filled')
xlabel('x(m)')
ylabel('range')
zlabel('y(m)')
% xlim([-2 2])
ylim([0 10])
% zlim([-2 2])
title('Range 3D scatter plot (meter)')

timeCostTotal = toc;
disp(['---- Total time cost is ', num2str(timeCostTotal/60), 'min ----'])