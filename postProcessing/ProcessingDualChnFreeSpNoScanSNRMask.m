% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% author: @xgao
% Velodyne Lidar

clc; close all; clear;

%% Load data
rangeRef = [1.8 1.8 10 10]; 
n = input('Enter a data set number: ');

switch n
    case 1
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_1point8m_Trigger.mat';
        opticalPathDly = 6;
    case 2
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_I.mat'; % added delay line 1m
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL_1point8m_Trigger.mat';
        opticalPathDly = 6 + 6;
    case 3
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_I.mat';
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_10point0m_Trigger.mat';
        opticalPathDly = 6;
    case 4
        filenameI = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_I.mat'; % added delay line 20m
        filenameQ = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_Q.mat';
        filenameTrigger = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\12202021\noScan_DL20m_10point0m_Trigger.mat';
        opticalPathDly = 6+60;
    otherwise
        disp('Wrong entry, please choose between 1 and 4')
end
disp(['Reference range is ', num2str(rangeRef(n)), 'm'])

load (filenameI)
load (filenameQ)
load (filenameTrigger)

%% Initialize data
% Integrate drive signal by subinterval to get chirp vs. time
Fs = 1e9;
N = length(Ch4s);
driveSig = Ch4s; 
DriveSigIntgral = zeros(1,N);
DriveSigIntgral(1) = 2*pi*driveSig(1);
for i = 2:N
    DriveSigIntgral(i) = DriveSigIntgral(i-1) + 2*pi*(driveSig(i)+driveSig(i-1))/2/Fs;
end


t = 0:1/Fs:(N-1)/Fs; 
figure
subplot(211)
plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
subplot(212)
plot(t*1e6, DriveSigIntgral); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive chirp')

[minVal, tLaser] = min(DriveSigIntgral);
if tLaser > N/2
   [minVal, tLaser] = min(-DriveSigIntgral);
end

I_Raw = Ch1s;
Q_Raw = Ch2s;

figure
subplot(221)
plot(t*1e6, I_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel I)')
subplot(222); spectrogram(I_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

subplot(223)
plot(t*1e6, Q_Raw); xlabel('time(us)'); ylabel('voltage(v)'); title('Short delay line signal (Channel Q)')
subplot(224); spectrogram(Q_Raw, 2048, 2000, 2048, Fs, 'yaxis')
colorbar('off')

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.5e9;             % bandwidth 1.5GHz
fs = 1e9;               % sampling rate, choice of range 1-3GHz
rangeMax = 100;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
sampleCropRate = 0.1;   % (4K FFT) 10% portion cropped at head and tail of chirp
% sampleCropRate = 0.2286;   % (2K FFT) 22.86% portion cropped at head and tail of chirp
% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = sampleCropRate*tUpChirp;         % up-chirp head non-linear portion
t3p = sampleCropRate*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = sampleCropRate*tDownChirp;       % down-chirp head non-linear portion
t3n = sampleCropRate*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% FFT parameter
fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K

disp('--------References--------')
disp(['Actual FFT block size = ', num2str(t2p*fs), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])
disp(['Reference beat tone is ', num2str((rangeRef(n)*2 + opticalPathDly)/c*alpha_up/1e6), 'MHz'])
%% Perform segmented FFT, calculate range and velocity
sigMx = I_Raw + 1i*Q_Raw;
sigMxSeg = sigMx(tLaser:end);
tSeg = t(tLaser:end);
numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/Fs;

% converting delay time to samples
t0pSps = round(t0p*fs);
t1pSps = round(t1p*fs);
t2pSps = round(t2p*fs); % fft up-chirp
t3pSps = round(t3p*fs);
t0nSps = round(t0n*fs);
t1nSps = round(t1n*fs);
t2nSps = round(t2n*fs); % fft down-chirp
t3nSps = round(t3n*fs);
tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

mkrFFTupS = t0pSps+t1pSps+1;
mkrFFTupE = mkrFFTupS+t2pSps;
mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
mkrFFTdnE = mkrFFTdnS+t2nSps;

delaySpsStartup = t0pSps+t1pSps;
delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

numSpsTotal = length(sigMxSeg);
numSpsFFT = fftSize;
% snrUp = [];
% snrDn = [];
% snrUpdB = [];
% snrDndB = [];
% fp = [];
% fn = [];
% rangeEst = [];
% vEst = [];
% fftBlkUpChirp = [];
% fftBlkDnChirp = [];
% fftBlkUpChirpIdx = [];
% fftBlkDnChirpIdx = [];
% freqFWHMupChirp = [];
% freqFWHMdnChirp = [];
cycleIdx = 1:tSpsFullCycle:numSpsTotal;
cycleLen = length(cycleIdx)-1;

% Verification plot - Highlight delayed and valid FFT regions for verification
% figure(4)
% subplot(211)
% plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
% subplot(212)
% plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
% xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')
fp = zeros(1,cycleLen); % pre-allocate space, speeds things up
fn = zeros(1,cycleLen);
fpMag = zeros(1,cycleLen);
fnMag = zeros(1,cycleLen);
rangeEst = zeros(1,cycleLen); % pre-allocate space, speeds things up
vEst = zeros(1,cycleLen); 

snrUp = zeros(1,cycleLen);
snrDn = zeros(1,cycleLen);
snrUpdB = zeros(1,cycleLen);
snrDndB = zeros(1,cycleLen);

freqFWHMupChirp = zeros(1,cycleLen);
freqFWHMdnChirp = zeros(1,cycleLen);

%% Sliced FFT
for i = 1:length(cycleIdx)-1
    fftBlkUpChirp = zeros(1,t2pSps); % pre-allocate space for fft chunks (total len = 3333)
    fftBlkDnChirp = zeros(1,t2nSps);
    fftBlkUpChirpIdx = zeros(1,t2pSps);
    fftBlkDnChirpIdx = zeros(1,t2nSps); 
    RunningCtrUp = 1;
    RunningCtrDn = 1;
    sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
    for j = 1:length(sigMx_subSet)
        if j >= mkrFFTupS && j < mkrFFTupE
%             fftBlkUpChirp = [fftBlkUpChirp sigMx_subSet(j)];
%             fftBlkUpChirpIdx = [fftBlkUpChirpIdx cycleIdx(i)+j-1];
            fftBlkUpChirp(RunningCtrUp) = sigMx_subSet(j);
            fftBlkUpChirpIdx(RunningCtrUp) = cycleIdx(i)+j-1;
            RunningCtrUp = RunningCtrUp + 1;
        elseif j >= mkrFFTdnS && j < mkrFFTdnE
%             fftBlkDnChirp = [fftBlkDnChirp sigMx_subSet(j)];
%             fftBlkDnChirpIdx = [fftBlkDnChirpIdx cycleIdx(i)+j-1];
            fftBlkDnChirp(RunningCtrDn) = sigMx_subSet(j);
            fftBlkDnChirpIdx(RunningCtrDn) = cycleIdx(i)+j-1;        
            RunningCtrDn = RunningCtrDn + 1;
        end
    end
%         figure(4)
%         subplot(211)
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, real(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - I', 'FFT region')
%         subplot(212)
%         plot(tSeg(fftBlkDnChirpIdx(1):fftBlkDnChirpIdx(end))*1e6, imag(fftBlkDnChirp),'color',[0.85,0.33,0.10]);
%         plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, imag(fftBlkUpChirp),'color',[0.85,0.33,0.10]);
%         legend('Mixed signal - Q', 'FFT region')        
        
        yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));
        yFFTdnChirp = fftshift(fft(fftBlkDnChirp, fftSize));
        
        % Masking
        maskFreqsUpChirpLow = [-80e6 8e6]; % masking frequency upper bound
        maskFreqsDnChirpLow = [-8e6 80e6];
        maskIdxUpChirpLow = round(maskFreqsUpChirpLow/Fs * fftSize);
        maskIdxDnChirpLow = round(maskFreqsDnChirpLow/Fs * fftSize);
        yFFTupChirp(fftSize/2+maskIdxUpChirpLow(1) : fftSize/2+maskIdxUpChirpLow(2)) = 0;
        yFFTdnChirp(fftSize/2+maskIdxDnChirpLow(1): fftSize/2+maskIdxDnChirpLow(2)) = 0;
%         maskFreqsHigh = 100e6;
%         maskIdxHigh = round(maskFreqsHigh/Fs * fftSize);
%         yFFTupChirp(fftSize/2+maskIdxHigh:end) = 0;
%         yFFTdnChirp(1:fftSize/2-maskIdxHigh) = 0;
        f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
        f = f/numSpsFFT*fs;                 % convert in frequency
        
        figure(5) % original spectrum
%         subplot(floor(numChirpCyc),1,i)
        plot(f/1e6, abs(yFFTupChirp)); hold on; plot(f/1e6,abs(yFFTdnChirp)); 
        xlabel('Frequency (MHz)'); ylabel('Magnitude')

        [fpMag, fpIdx] = max(abs(yFFTupChirp));
        [fnMag, fnIdx] = max(abs(yFFTdnChirp));
        fpCur = f(fpIdx);
        fnCur = f(fnIdx);
        
        % FWHM
       [widthUpChirp] = fwhm(abs(yFFTupChirp), fftSize, Fs, 0);
       [widthDnChirp] = fwhm(abs(yFFTdnChirp), fftSize, Fs, 0);
%        freqFWHMupChirp = [freqFWHMupChirp widthUpChirp];
%        freqFWHMdnChirp = [freqFWHMdnChirp widthDnChirp];
       freqFWHMupChirp(i) = widthUpChirp;
       freqFWHMdnChirp(i) = widthDnChirp;

        % SNR calculation
%         noiseBandIdx = round([-200e6 -50e6 50e6 200e6]/Fs*fftSize); 
        noiseBandIdx = round([-400e6 -150e6 150e6 400e6]/Fs*fftSize); 
        snrUpCur = fpMag/rms(abs(yFFTupChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));
        snrDnCur = fnMag/rms(abs(yFFTdnChirp([fftSize/2+noiseBandIdx(1):fftSize/2+noiseBandIdx(2),fftSize/2+noiseBandIdx(3):fftSize/2+noiseBandIdx(4)])));
%         snrUp = [snrUp snrUpCur];
%         snrDn = [snrDn snrDnCur];
%         snrUpdB = [snrUpdB 10*log10(snrUpCur)];
%         snrDndB = [snrDndB 10*log10(snrDnCur)];
        snrUp(i) = snrUpCur;
        snrDn(i) = snrDnCur;            
        snrUpdB(i) = 10*log10(snrUpCur);
        snrDndB(i) = 10*log10(snrDnCur);
%         subplot(floor(numChirpCyc),1,i)
%         plot(fpCur/1e6, fpMag,'g*'); text(fpCur/1e6, fpMag,['fp = ', num2str(fpCur/1e6),'MHz'])
%         plot(fnCur/1e6, fnMag,'g^'); text(fnCur*1.2/1e6, fnMag,['fn = ', num2str(fnCur/1e6), 'MHz'])
%         legend('Up-chirp FFT', 'Down-chirp FFT','Up-chirp peak', 'Down-chirp peak')
%         rangeCur = (abs(fpCur) + abs(fnCur))*c/4/alpha_up; % if no length difference in optical paths
        rangeCur = ((fpCur - fnCur)*c/2/alpha_up - opticalPathDly)/2; % considering optical length differences
        vCur = -(fpCur + fnCur)*lambda/4;
        % Logging estimation results
%         fp = [fp fpCur];
%         fn = [fn fnCur];
%         rangeEst = [rangeEst rangeCur];
%         vEst = [vEst vCur];
        fp(i) = f(fpIdx);
        fn(i) = f(fnIdx);
        fpMag(i) = fpMagCur;
        fnMag(i) = fnMagCur;
        rangeEst(i) = rangeCur;
        vEst(i) = vCur;
        % Investigation on Masking         
        figure(6) % stack FFTs 
        subplot(211)
        plot(f/1e6, abs(yFFTupChirp)); hold on;
        xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Up-chirp FFT of all cycles')
        subplot(212)
        plot(f/1e6,abs(yFFTdnChirp)); hold on;
        xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Down-chirp FFT of all cycles')
        
        fftBlkUpChirp = [];
        fftBlkDnChirp = [];
        fftBlkUpChirpIdx = [];
        fftBlkDnChirpIdx = [];
end
disp('--------Estimation Results--------')
disp(['Estimated range is ', num2str(rangeEst), 'm'])
disp(['Estimated velocity is ', num2str(vEst), 'm/s'])
disp(['Mean range is ', num2str(mean(rangeEst)), 'm'])
disp(['Mean velocity is ', num2str(mean(vEst)), 'm/s'])
disp(['Mean, Median, std fp is ', num2str(mean(fp)/1e6), ' ', num2str(median(fp)/1e6), ' ', num2str(std(fp)/1e6), ' MHz'])
disp(['Mean, Median, std fn is ', num2str(mean(fn)/1e6), ' ', num2str(median(fn)/1e6), ' ', num2str(std(fn)/1e6),' MHz'])
disp(['Mean FWHM of up-chirp is ', num2str(mean(freqFWHMupChirp)/1e6), 'MHz'])
disp(['Mean FWHM of down-chirp is ', num2str(mean(freqFWHMdnChirp)/1e6), 'MHz'])
%% Distribution plots

% 1. Histogram of beat tone
% edgesUpChirpBeatTone = [-500:10:500]; % 10MHz wide bins
% edgesdnChirpBeatTone = [-500:10:500]; % 10MHz wide bins
% edgesUpChirpBeatTone = [5:0.5:15]; % 10MHz wide bins
% edgesDnChirpBeatTone = [-15:0.5:-5]; % 10MHz wide bins
edgesDnChirpBeatTone = 70:0.5:90; % 10MHz wide bins
edgesUpChirpBeatTone = -90:0.5:-70; % 10MHz wide bins
figure
subplot(211);histogram(fp/1e6,edgesUpChirpBeatTone,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution')
subplot(212); histogram(fn/1e6,edgesDnChirpBeatTone,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution')

% 2. Histogram of SNR
figure
subplot(311)
plot(snrUpdB);
hold on;
plot(snrDndB);
xlabel('up/down chirp cycles')
ylabel('SNR (dB)')
legend('upchirp SNR', 'downchirp SNR')

disp(['Mean, Median, std SNR of up-chirp is ', num2str(mean(snrUpdB)), ' ', num2str(median(snrUpdB)), ' ', num2str(std(snrUpdB)), ' dB'])
disp(['Mean, Median, std SNR of down-chirp is ', num2str(mean(snrDndB)), ' ', num2str(median(snrDndB)), ' ', num2str(std(snrDndB)), ' dB'])
subplot(312);histogram(snrUpdB,'EdgeColor',[1 1 1])
xlabel('SNR (dB)');title('Upchirp FFT SNR distribution')

subplot(313); histogram(snrDndB,'EdgeColor',[1 1 1])
xlabel('SNR (dB)');title('Downchirp FFT SNR distribution')

% 3. Histogram of beat tone FWHM
figure
subplot(211)
histogram(freqFWHMupChirp/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone FWHM')
subplot(212)
histogram(freqFWHMdnChirp/1e6,'EdgeColor',[1 1 1])
xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone FWHM')

