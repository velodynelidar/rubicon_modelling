% Statistical Results Processing
close all;
% Specify workspace directory of saved output .mat files
% userpath ('C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\01122022\results\snr')
do_statisticalPlots = input('Do statistical plot? Y(1)/N(0): ');
do_beatTonePlot = input('Do beat tone plot? Y(1)/N(0): ');

totalSeg = 15;
prevSpLast = 0;
% snrStatisticResults = [];
snrStatisticResults = zeros(totalSeg,10);
for segNum = 1:totalSeg
% for segNum = 5
    outputClusterFilename =['saveOutput_Seg',num2str(segNum),'.mat'];
    load (outputClusterFilename);

    if do_statisticalPlots == 1
        % 1. Histogram of SNR
        % Note: when masked or threshold-applied, SNR is set to zero. The zero-SNRs
        % are not plotted or analyzed below
        figure
        subplot(311)
        plot(nonzeros(snrUpdB));
        hold on;
        plot(nonzeros(snrDndB));
        xlabel('up/down chirp cycles w/ non-zero SNR')
        ylabel('SNR (dB)')
        legend('upchirp SNR', 'downchirp SNR')
        
        subplot(312);histogram(nonzeros(snrUpdB),'EdgeColor',[1 1 1])
        xlabel('SNR (dB)');title('Upchirp FFT SNR distribution (w/ thresholding)')
        subplot(313); histogram(nonzeros(snrDndB),'EdgeColor',[1 1 1])
        xlabel('SNR (dB)');title('Downchirp FFT SNR distribution (w/ thresholding)')
    
        snrStatisticResults(segNum,:) = [min(nonzeros(snrUpdB)), max(nonzeros(snrUpdB)), min(nonzeros(snrDndB)), max(nonzeros(snrDndB)), mean(nonzeros(snrUpdB)), median(nonzeros(snrUpdB)), std(nonzeros(snrUpdB)), mean(nonzeros(snrDndB)), median(nonzeros(snrDndB)), std(nonzeros(snrDndB))];
        disp(['SegNum #', num2str(segNum)])
        disp(['Range of valid SNR of up-chirp is ' num2str(min(nonzeros(snrUpdB))), ' to ',  num2str(max(nonzeros(snrUpdB))), ' dB'])
        disp(['Mean, Median, std valid SNR of up-chirp is ', num2str(mean(nonzeros(snrUpdB))), ' ', num2str(median(nonzeros(snrUpdB))), ' ', num2str(std(nonzeros(snrUpdB))), ' dB'])
        disp(['Range of valid SNR of down-chirp is ' num2str(min(nonzeros(snrDndB))), ' to ',  num2str(max(nonzeros(snrDndB))), ' dB'])
        disp(['Mean, Median, std valid SNR of down-chirp is ', num2str(mean(nonzeros(snrDndB))), ' ', num2str(median(nonzeros(snrDndB))), ' ', num2str(std(nonzeros(snrDndB))), ' dB'])
    
    
        % 2. Beat tone magnitude vs frequency
        figure
        subplot(221); plot(fp/1e6, fpMag,'o'); xlabel('Frequency (MHz)'); ylabel('Up-chirp'); title('w/o thresholding')
        subplot(223); plot(fn/1e6, fnMag,'o'); xlabel('Frequency (MHz)'); ylabel('Down-chirp');title('w/o thresholding')
        subplot(222); plot(fpOut/1e6, fpMag,'go'); xlabel('Frequency (MHz)'); title('w/ thresholding');xlim([-fs/2 fs/2-1]);
        subplot(224); plot(fnOut/1e6, fnMag,'go'); xlabel('Frequency (MHz)'); title('w/ thresholding');xlim([-fs/2 fs/2-1]);
        % 3. Beat tone histogram
        figure
        subplot(221);histogram(fp/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution')
        subplot(223); histogram(fn/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution')
        subplot(222);histogram(fpOut/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution with thresholding')
        subplot(224); histogram(fnOut/1e6,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Down-chirp FFT beat tone distribution with thresholding')
    else
    end
    
    if do_beatTonePlot == 1
        figure(99)
        spsTotalCur = length(fpOut);
        subplot(211);plot(prevSpLast+1:prevSpLast+spsTotalCur,fpOut/1e6,'o'); hold on;
        title('Up-chirp beat tones');xlabel('samples'); ylabel('Beat tone freq (MHz)')
        subplot(212);plot(prevSpLast+1:prevSpLast+spsTotalCur,fnOut/1e6,'o'); hold on;
        title('Down-chirp beat tones');xlabel('samples'); ylabel('Beat tone freq (MHz)')
        prevSpLast = prevSpLast + spsTotalCur;
    else
    end
end