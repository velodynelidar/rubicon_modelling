% ADC resolution improvement
clear; close all; clc;

pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\';
filenameCh3 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch3.mat']);
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);

load(filenameCh3);
load(filenameCh4);

% start from Frame #2
x = Ch4s(2,1:length(Ch4s)/10);
N = length(x);
sp = 1:N;

% method 1 - not the best way?!
% decimation by interval
intvl = 2^15;
index = 1:intvl:N;

x_decimated1 = x(index);

figure
plot(x); hold on;
plot(sp(index), x_decimated1,'r*-');
legend('original x','decimated x')


% method 2
% decimation by matlab function
decimateRate = 2^12;
x_decimated2 = decimate(x,decimateRate);
x_interp = zeros(1,N);
x_interp = interp(x_decimated2,round(N/length(x_decimated2)));

figure
plot(x); hold on;
plot(x_interp(1:N),'g*-');
legend('original x','decimated x')
