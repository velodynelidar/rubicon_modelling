tic;
pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\';
% filenameCh3 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch3.mat']);
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);

% load(filenameCh3);
load(filenameCh4);

M = size(Ch4s,1);
N = size(Ch4s,2);

% decimation-interpolation operation to smooth X and Y - potentially increase resolution
decimateRate = 2^12;

Ch4s_interp = zeros(M,N);
Ch4s_decimated = zeros(M,ceil(size(Ch4s,2)/decimateRate));

for p = 1:15
    Ch4s_decimated(p,:) = decimate(Ch4s(p,:),decimateRate);
    cur_Ch4s_interp = interp(Ch4s_decimated(p,:), round(N/length(Ch4s_decimated(p,:))));
    Ch4s_interp(p,:) = cur_Ch4s_interp(1:N); 
end

timeCost = toc;
disp(['Interpolating all X costs ', num2str(timeCost/60), 'min']);
