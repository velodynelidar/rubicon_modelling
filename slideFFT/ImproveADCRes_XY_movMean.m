tic;
pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\';
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);

load(filenameCh4);

M = size(Ch4s,1);
N = size(Ch4s,2);

% moving average operation to smooth X - potentially increase resolution
movMeanWinLen = 2^12;

Ch4s_movMean = zeros(M,N);

for p = 1:15
    Ch4s_movMean(p,:) = movmean(Ch4s(p,:),movMeanWinLen);
end

timeCost = toc;
disp(['Moving average all X costs ', num2str(timeCost/60), 'min']);
