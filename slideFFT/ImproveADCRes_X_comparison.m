% comparison of X scan data before and after modification

pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\';
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);
load(filenameCh4);

pathHeaderPreload = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\10192021_Reba\';
filenameCh4_movMean = ([pathHeaderPreload 'XYscan_stitch_10deg_tilt_500MHz_Ch4s_movMean_wWinLen4096.mat']);
filenameCh4_interp = ([pathHeaderPreload 'XYscan_stitch_10deg_tilt_500MHz_Ch4s_interp_wDeciRate4096.mat']);
load(filenameCh4_movMean);
load(filenameCh4_interp);

N = length(Ch4s);
Fs = 500e6;
t = 0:1/Fs:(N-1)/Fs; 
zoominFactor = 800;

figure
subplot(211)
plot(t*1e6, Ch4s(2,:)); xlabel('time(us)'); ylabel('voltage(v)'); 
title('X scan comparison')
hold on
plot(t*1e6, Ch4s_movMean(2,:));
plot(t*1e6, Ch4s_interp(2,:));
legend('raw','move mean','interpolation')
subplot(212)
plot(t(1:N/zoominFactor)*1e6, Ch4s(2,1:N/zoominFactor)); xlabel('time(us)'); ylabel('voltage(v)'); 
title(['zoom-in ',num2str(zoominFactor), ' times'])
hold on
plot(t(1:N/zoominFactor)*1e6, Ch4s_movMean(2,1:N/zoominFactor));
plot(t(1:N/zoominFactor)*1e6, Ch4s_interp(2,1:N/zoominFactor));
legend('raw','move mean','interpolation')