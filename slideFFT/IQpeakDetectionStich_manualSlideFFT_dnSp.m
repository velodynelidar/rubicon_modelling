clear; close all; clc;
tic; 
do_timeFreqPlot = input('Do beat tone plot for all 15 frames? Y(1) or N(0)');
X_modified = input('Which X modification set to use? None (0), Movmean(1) or Interpolation(2)');


% Load raw Reba data
% pathHeader = 'U:\Coherent Lidar\Data\10192021\'; % slow access path on server
pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\'; % local path on Xiaomeng's PC
% pathHeader = 'C:\Users\coherentlab01\Documents\xgao\realData\10192021\'; %local path on coherent lab PC
filenameCh1 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch1.mat']);
filenameCh2 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch2.mat']);
filenameCh3 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch3.mat']);
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);

load(filenameCh1);
load(filenameCh2);
load(filenameCh3);
load(filenameCh4);

% Decimation on raw
deciRate = 24;
Ch1s_deci = zeros(15,round(25e6/deciRate));
Ch2s_deci = zeros(15,round(25e6/deciRate));
Ch3s_deci = zeros(15,round(25e6/deciRate));
Ch4s_deci = zeros(15,round(25e6/deciRate));
for d = 1:15
    Ch1s_deci(d,:) = downsample(Ch1s(d,:), deciRate);
    Ch2s_deci(d,:) = downsample(Ch2s(d,:), deciRate);
    Ch3s_deci(d,:) = downsample(Ch3s(d,:), deciRate);
    Ch4s_deci(d,:) = downsample(Ch4s(d,:), deciRate);
end
% Load modified X scan data

% pathHeaderPreload = 'U:\Coherent Lidar\Data\10192021_Reba\'; % slow access path on server
pathHeaderPreload = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Rubicon\realData\10192021_Reba\';% local path on Xiaomeng's PC
% pathHeaderPreload = 'C:\Users\coherentlab01\Documents\xgao\realData\10192021\';  %local path on coherent lab PC
filenameCh4_movMean = ([pathHeaderPreload 'XYscan_stitch_10deg_tilt_500MHz_Ch4s_movMean_wWinLen4096.mat']);
filenameCh4_interp = ([pathHeaderPreload 'XYscan_stitch_10deg_tilt_500MHz_Ch4s_interp_wDeciRate4096.mat']);

M = size(Ch1s_deci,1);
N = size(Ch1s_deci,2);

% movemean operation to smooth X and Y - potentially increase resolution
if X_modified == 0
    disp 'no modification on X'
elseif X_modified == 1
    disp('Loading moving average of X scan data...')
    load(filenameCh4_movMean)
    % Decimation on movMean data
    Ch4s_movMeanDeci = zeros(15,round(25e6/deciRate));
    for d = 1:15
        Ch4s_movMeanDeci(d,:) = downsample(Ch4s_movMean(d,:), deciRate);
    end
elseif X_modified == 2
    disp('Loading interpolation of X scan data...')
    load(filenameCh4_interp)
    % Decimation on movMean data
    Ch4s_interpDeci = zeros(15,round(25e6/deciRate));
    for d = 1:15
        Ch4s_interpDeci(d,:) = downsample(Ch4s_interp(d,:), deciRate);
    end
end



% fft parameters
Fs = 500e6/deciRate;
numSamples = length(Ch1s_deci);

overlapCount = 0;
% every 8 μs: overlap = -3072
% every 4 μs: overlap = -1024
% every 2 μs: overlap = 0
% every 1 μs: overlap = 512
% every ? μs: overlap = 812

fftSize = 1024;
fftSegIdx = 1:(fftSize - overlapCount):numSamples-fftSize;
validFrameStart = length(fftSegIdx);
F = -fftSize/2:fftSize/2-1;
F = F/fftSize*Fs;

totalTime = numSamples/Fs;
tStep = totalTime/length(fftSegIdx);
T = tStep/2:tStep:totalTime; 

scalingFactor = length(fftSegIdx)/24414;
% Display results
disp('Processing parameters:')
disp(['Overlap samples: ', num2str(overlapCount)])
disp(['FFT counts per segment (max=25M): ', num2str(length(fftSegIdx)), '(',num2str(scalingFactor),'x)'])
disp(['FFT operation done every: ', num2str((fftSegIdx(2)-fftSegIdx(1))/fftSize*2), 'μs'])

% reinstate hamming window
w = hamming(fftSize)';
% w = rectwin(fftSize)';
%% STFT
for k = 1:size(Ch1s_deci,1)

    complexSig_IQs = Ch1s_deci(k,:)+1i*Ch2s_deci(k,:);
%     % original approach using STFT. NOTE: no need STFT results as reference, just use brutal force FFT
%     [S,F,T]= stft(complexSig_IQs,Fs,'Window',rectwin(fftSize), 'OverlapLength',0,'FFTLength',fftSize);%IQ
    
    % Manual slide FFT
    S = zeros(fftSize, length(fftSegIdx));
    for i = 1:length(fftSegIdx)
        S(:,i) = fftshift(fft(w.*complexSig_IQs(fftSegIdx(i):fftSegIdx(i)+fftSize-1)));
    end
    
    figure(1)
    imagesc(T/1e-3,F/1e6,(abs(S(:,:,1)))); % Display Spectral Temporal plot (freq vs. time) with scaled colors 
    ylabel('Frequency(MHz)'); xlabel('Time(ms)'); title('Spectral Temporal plot'); set(gca,'FontSize',15); hold on

    %% peak detection for beat tone (without fftshift)
    threshold = 0.45;
    sizeFFTs = size(S); % outputs FFT length * number of FFTs in total (=25e6/1024*15)

    % masking mirror doppler frequency band
    mask_band = (1e6/(Fs/1024));
    mask = 1024/2-round(mask_band/2):1024/2+round(mask_band/2);
    S_masked = S;
    S_masked (mask+1,:) = 0; % offset mask by 1 due to loc = 513 is zero.
    % EXPLANATION
    % if fft index (after mirror) is -512 ~ 511, then left half is
    % -512 ~ -1 (total 512x), right half 0 ~ 511 (total 512x). If pick
    % 1024/2 = 512 loc, its actually -1, not 0 (DC). 
    % 1024/2 would correspond to DC only when fft index changed to -511 ~ 512.
    
    clear S;
    % Peak detection and thresholding and interference masking
    peakval = zeros(1,sizeFFTs(2));
    peakloc = zeros(1,sizeFFTs(2));
    interferenceFreq = 15e6; %  +-15MHz interference band
    interferenceIdx = round(interferenceFreq/Fs*fftSize);
    for h = 1:sizeFFTs(2)
         [peakval(h),peakloc(h)] = max(abs(S_masked(:,h)));
         if peakval(h) < threshold
             peakloc(h) = 1024/2+1;
         end
         if peakloc(h) > interferenceIdx+1024/2+1 || peakloc(h) < -interferenceIdx+1024/2+1 % +-15MHz band
             peakloc(h) = 1024/2+1;
         end
    end

    clear S_masked;
    if do_timeFreqPlot
        figure(2) % plot time-beat tone plot of all
        subplot(size(Ch1s_deci,1)/3,3,k)
        plot(T/1e-3,F(peakloc)/1e6)
        ylabel('Frequency(MHz)'); xlabel('Time(ms)')
        title(['Doppler beat tones of frame #', num2str(k)])
        set(gca,'FontSize',10); 
    else
    end

    %% xy scan
%     X_down = Ch4s(k,1024/2:1024:length(Ch4s));
%     Y_down = Ch3s(k,1024/2:1024:length(Ch3s));


    Y_down = Ch3s_deci(k, fftSegIdx+fftSize/2-1);
    
    if X_modified == 0
        X_down = Ch4s_deci(k, fftSegIdx+fftSize/2-1);
    elseif X_modified == 1
        X_down = Ch4s_movMeanDeci(k, fftSegIdx+fftSize/2-1);
    elseif X_modified == 2
        X_down = Ch4s_interpDeci(k, fftSegIdx+fftSize/2-1);
    end

    X((k-1)*sizeFFTs(2)+1:k*sizeFFTs(2)) = X_down; % concatenating X_down into large X.
    Y((k-1)*sizeFFTs(2)+1:k*sizeFFTs(2)) = Y_down;
    beat((k-1)*sizeFFTs(2)+1:k*sizeFFTs(2)) = F(peakloc)/1e6;
    disp(['Seg# ', num2str(k), ' done.'])
end

clear Ch1s_deci Ch2s_deci Ch3s_deci Ch4s_deci Ch1s Ch2s Ch3s Ch4s complexSig_IQs;

%% normalize X and Y, then scatter plot 
X_deg = (X - mean(X))/((max(X)-min(X))/2)*5;
Y_deg = (Y - mean(Y))/((max(Y)-min(Y))/2)*5;

figure
scatter(X_deg,Y_deg,10,beat,'filled','s')
axis square
set(gca,'FontSize',10);
title('Raw scatter plot'); xlabel('x (degree)'); ylabel('y (degree)')
colorbar

% scatter plot on valid frames (skip frame #1, total equivalent FFTs in frame #1 = length(fftSegIdx) )
figure
scatter(X_deg(validFrameStart : end),Y_deg(validFrameStart : end),2,beat(validFrameStart : end),'filled','s')
axis square
set(gca,'FontSize',10);
title(['(Dnsp. by' num2str(deciRate), ') FFT every ', num2str((fftSegIdx(2)-fftSegIdx(1))/fftSize*2), 'μs (overlap=',num2str(overlapCount),', plot starts@FFT#', num2str(validFrameStart),')']);
xlabel('x (degree)'); ylabel('y (degree)');
xlim([-5 5]);ylim([-5 5]);
colorbar
colormap(redblue(20))

% zoom in view
figure
subplot(211)
validFrameStart = length(fftSegIdx)+1;
scatter(X_deg(validFrameStart : end),Y_deg(validFrameStart : end),2,beat(validFrameStart : end),'filled','s')
axis square
set(gca,'FontSize',10);
xlabel('x (degree)'); ylabel('y (degree)');
% title(['Scatter plot start from FFT#', num2str(validFrameStart)]); 
title(['(Dnsp. by' num2str(deciRate), ') FFT every ', num2str((fftSegIdx(2)-fftSegIdx(1))/fftSize*2), 'μs (xlim[-1 3.5],ylim[-5.1 -3.5])']); 
xlim([-1 3.5]);ylim([-5.1 -3.5]);
colorbar
colormap(redblue(20))

%% find # of points within the zoom-in block (w/ verification plot)
X_boundLo = -1;
X_boundHi = 3.5;
Y_boundLo = -5.1;
Y_boundHi = -3.5;

X_deg_largerThanBoundLo = find(X_deg(validFrameStart : end) > X_boundLo);
X_deg_smallerThanBoundHi = find(X_deg(validFrameStart : end) < X_boundHi);
X_deg_withinBound =  intersect(X_deg_largerThanBoundLo, X_deg_smallerThanBoundHi);

Y_deg_largerThanBoundLo = find(Y_deg(validFrameStart : end) > Y_boundLo);
Y_deg_smallerThanBoundHi = find(Y_deg(validFrameStart : end) < Y_boundHi);
Y_deg_withinBound =  intersect(Y_deg_largerThanBoundLo, Y_deg_smallerThanBoundHi);

totalIndicesWithinBlock = intersect(X_deg_withinBound, Y_deg_withinBound);
totalPointsCountWithinBlock = length(find(abs(beat(totalIndicesWithinBlock))>0));
disp(['Total # points within block xlim[-1~3.5]ylim[-5.1~-3.5] is ', num2str(totalPointsCountWithinBlock)]);

subplot(212)
scatter(X_deg(validFrameStart+totalIndicesWithinBlock),Y_deg(validFrameStart+totalIndicesWithinBlock ),2,beat(validFrameStart+totalIndicesWithinBlock),'filled','s')
axis square
set(gca,'FontSize',10);
xlabel('x (degree)'); ylabel('y (degree)');
% title(['Scatter plot start from FFT#', num2str(validFrameStart)]); 
title(['(Dnsp. by' num2str(deciRate), ') FFT every ', num2str((fftSegIdx(2)-fftSegIdx(1))/fftSize*2), 'μs (Verification Plot for X/Y Limited Block)']); 
xlim([-1 3.5]);ylim([-5.1 -3.5]);
colorbar
colormap(redblue(20))


runTimeTotal = toc;
disp(['Runtime total = ', num2str(runTimeTotal/60), 'min'])
