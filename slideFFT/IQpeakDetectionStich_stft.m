clear; close all; clc;
tic;
do_timeFreqPlot = input('Do beat tone plot for all 15 frames? Y(1) or N(0)');
zero_beatOutlier = input('Force zero outlier beat tone (for 1000 overlap only)? Y(1) or N(0)');

pathHeader = 'C:\Users\xgao\Box\Workspace\CoherentLidar\Reba\Reba Doppler Shift Test\10192021\';
filenameCh1 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch1.mat']);
filenameCh2 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch2.mat']);
filenameCh3 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch3.mat']);
filenameCh4 = ([pathHeader 'XYscan_stitch_10deg_tilt_500MHz_Ch4.mat']);

load(filenameCh1);
load(filenameCh2);
load(filenameCh3);
load(filenameCh4);
 
% load('XYscan_stitch_10deg_tilt_500MHz_Ch1.mat');
% load('XYscan_stitch_10deg_tilt_500MHz_Ch2.mat');
% load('XYscan_stitch_10deg_tilt_500MHz_Ch3.mat');
% load('XYscan_stitch_10deg_tilt_500MHz_Ch4.mat');
Fs = 500e6;
RecordLength= length(Ch1s);
frequency = [-RecordLength/2:RecordLength/2-1]*Fs/RecordLength;

% XG added contents to accomondate different overlaps
numSamples = 25e6;
overlapCount = 0;
fftSize = 1024;
fftSegIdx = 1:(fftSize - overlapCount):numSamples-fftSize;
totalTime = numSamples/Fs;
tStep = totalTime/length(fftSegIdx);
T = tStep/2:tStep:totalTime; 


%% STFT
for k = 1:size(Ch1s,1)

    fftSize = 2^10;
    complexSig_IQs = Ch1s(k,:)+1i*Ch2s(k,:);
    [S,F,T]= stft(complexSig_IQs,Fs, 'Window',hamming(fftSize),'OverlapLength',overlapCount,'FFTLength',fftSize);%IQ

    figure(1)
    imagesc(T/1e-3,F/1e6,(abs(S(:,:,1)))); % Display Spectral Temporal plot (freq vs. time) with scaled colors 
    ylabel('Frequency(MHz)'); xlabel('Time(ms)'); set(gca,'FontSize',30); hold on
    %% peak detection for beat tone (without fftshift)
    threshold = 0.45;
    numFFTs = size(S); % outputs FFT length * number of FFTs in total (=25e6/1024*15)

    % masking mirror doppler frequency band
    mask_band = (1e6/(Fs/1024));
    mask = 1024/2-round(mask_band/2):1024/2+round(mask_band/2);
    S_search = S;
    S_search (mask,:) = 0;

    for h = 1:numFFTs(2)
         [peakval(h),peakloc(h)] = max(abs(S_search(:,h)));
         if peakval(h)<threshold
             peakloc(h) = 1024/2;
         end
    end
    if do_timeFreqPlot
        figure(2) % plot time-beat tone plot of all
        subplot(size(Ch1s,1)/3,3,k)
        plot(T/1e-3,F(peakloc)/1e6)
        ylabel('Frequency(MHz)'); xlabel('Time(ms)')
        title(['Doppler beat tones of frame #', num2str(k)])
        set(gca,'FontSize',10); 
    else
    end
     
    %% xy scan
%     X_down = Ch4s(k,1024/2:1024:length(Ch4s));
%     Y_down = Ch3s(k,1024/2:1024:length(Ch3s));
    X_down = Ch4s(k, fftSegIdx+fftSize/2-1);
    Y_down = Ch3s(k, fftSegIdx+fftSize/2-1);
    X((k-1)*numFFTs(2)+1:k*numFFTs(2)) = X_down; % concatenating X_down into large X.
    Y((k-1)*numFFTs(2)+1:k*numFFTs(2)) = Y_down;
    beat((k-1)*numFFTs(2)+1:k*numFFTs(2)) = F(peakloc)/1e6;
 end
 %% 
 
X_deg = (X - mean(X))/((max(X)-min(X))/2)*5;
Y_deg = (Y - mean(Y))/((max(Y)-min(Y))/2)*5;

figure
scatter(X_deg,Y_deg,10,beat,'filled','s')
axis square
set(gca,'FontSize',30);
xlabel('x (degree)')
ylabel('y (degree)')

% force zero outlier beat tone on valid frames (skip frame #1)
if zero_beatOutlier
    minFreqIdx = find(beat == min(beat));
    beat(minFreqIdx) = 0;
else
end

% scatter plot on valid frames (skip frame #1, total equivalent FFTs in frame #1 = length(fftSegIdx) )
figure
validFrameStart = length(fftSegIdx)+1;
scatter(X_deg(validFrameStart : end),Y_deg(validFrameStart : end),5,beat(validFrameStart : end),'filled','s')
axis square
set(gca,'FontSize',10);
xlabel('x (degree)')
ylabel('y (degree)')
title(['Scatter plot start from FFT#', num2str(validFrameStart)]); xlabel('x (degree)'); ylabel('y (degree)');
xlim([-5 5]);ylim([-5 5]);

% Display results
disp('Processing parameters:')
disp(['Overlap samples: ', num2str(overlapCount)])
disp(['FFT counts per segment (max=25M): ', num2str(length(fftSegIdx))])
runTimeTotal = toc;
disp(['Runtime total = ', num2str(runTimeTotal/60), 'min'])