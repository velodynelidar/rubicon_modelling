% FMCW lidar (Rubicon) dsp framework evaluation
% Created 12/14/2021 17:5756
% Modified 3/16/2022 for Hywire processing
% author: @xgao
% Velodyne Lidar

% External function Red Blue Colormap: redblue.m
% version 1.0.0.0 (1.47 KB) by Adam Auton

% System parameters            Value
% ----------------------------------
% Carrier frequency            193.55THz (3e8/1550e-9)
% Carrier frequency modified   250MHz
% Maximum target range (m)     100
% Range resolution (m)         0.15
% Maximum target speed (m/s)   111 (250mi/hr)
% Chirp time (microseconds)    5
% Chirp bandwidth (GHz)        1.5
% Maximum beat frequency (MHz) 200MHz
% Sample rate                  1GHz
% Reflective index for fiber   1.5

% The sweep time can be computed based on the time needed for the signal 
% to travel the unambiguous maximum range. In general, for an FMCW radar 
% system, the sweep time should be at least 5 to 6 times the round trip time.

clc; close all; clear;
tic
%% Load data
rangeRef = [7.62 0 7.62]; % reference range of the major target (placeholder, may remove)
n = input('Enter a data set number: ');
segTotal = input('Enter how many segments to run: '); % for wire detection, default is 1 segment
do_masking = input('Do masking? Y(1)/N(0): ');
do_infoPlot = input('Do information plot? Y(1)/N(0): ');
do_EstResultDisplay = input('Do estimation result display? Y(1)/N(0): ');
do_FFTplot = input('Do FFT plot with longer wait time? Y(1)/N(0): ');
% % % do_saveOutputData = input('Save output data? Y(1)/N(0): ');

%% Rubicon variables definition
% System parameter
fc = 3e8/1550e-9;       % actual carrier frequency of 1550nm laser (f = c/lambda)   
bw = 1.5e9;             % bandwidth 1GHz
fs = 2.5e9;               % sampling rate, choice of range 1-3GHz
% rangeMax = 100;         % Maximum target range 100m  
rangeMax = 67;         % Maximum target range 100m  
vMax = 111;             % Maximum target velocity 111m/s
c = 3e8;                % speed of light
lambda = c/fc;          % wavelength
sampleCropRate = 0.02;       % portion cropped at head and tail of chirp
% sampleCropRate = 0.0458;       % portion cropped at head and tail of chirp


% Chirp parameter
tUpChirp = 5e-6;            % up-chirp time span
tDownChirp = 5e-6;          % down-chirp time span
ftotalChirp = 1/(tUpChirp + tDownChirp);

% tMax = (tUpChirp + tDownChirp)*numChirpCyc;    % total time span
alpha_up = bw/tUpChirp;     % slope of up-chirp
alpha_down = bw/tDownChirp; % slope of down-chirp
t0p = rangeMax*2/c;         % tau_max, roundtrip time cost
t1p = sampleCropRate*tUpChirp;         % up-chirp head non-linear portion
t3p = sampleCropRate*tUpChirp;         % up-chirp tail non-linear portion
t2p = tUpChirp-t0p-t1p-t3p; % up-chirp FFT

t0n = rangeMax*2/c;         % tau_max, roundtrip time cost
t1n = sampleCropRate*tDownChirp;       % down-chirp head non-linear portion
t3n = sampleCropRate*tDownChirp;       % down-chirp tail non-linear portion
t2n = tDownChirp-t0n-t1n-t3n;% down-chirp FFT

% fftSize = 2^nextpow2(t2p*fs);% FFT size in sample points, runtime cal., choice of range 4K or 8K
fftSize = 8192;% FFT size in sample points, runtime cal., choice of range 4K or 8K
% slide FFT parameters config
overlapCount = fftSize - 1;
numSpsSliced = round(t2p*fs);
fftSegIdx = 1:(fftSize - overlapCount):numSpsSliced-fftSize+1; % index of slide FFT from 1 to total # of slide FFTs
% % Smart slide FFT parameters
slideFFTidx = 0:fftSize-1;
slideFFTcoeff = exp(1i*2*pi*slideFFTidx/fftSize)';

disp('--------References--------')
disp(['Actual FFT block size = ', num2str(numSpsSliced), ' -> FFT size = ', num2str(fftSize)])
disp(['Max beat tone = ', num2str(t0p*alpha_up/1e6), 'MHz (equivalent range = ', num2str(rangeMax), 'm)'])

%% Sweep segment # to run processing script
numFFTPtPerCycle_slideFFT = (numSpsSliced - fftSize + 1)/(fftSize - (fftSize-1));
outputPtMax = segTotal * round(25e6/((tUpChirp + tDownChirp)*fs)) * (numFFTPtPerCycle_slideFFT); % # of up/down chirps -> each chirp is a point if taking only 1 FFT

outputPtIdx_CurSegStart = 0;
outputClusterFilename  = [];

outputX = zeros(1,outputPtMax);
outputY = zeros(1,outputPtMax);
outputRange = zeros(1,outputPtMax);

outputX_meter = zeros(1,outputPtMax);
outputY_meter = zeros(1,outputPtMax);
outputRange_meter = zeros(1,outputPtMax);

for segNum = 1:segTotal
%% Load data
    switch n
        case 1
            filenameI = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\I03152022.mat');
            filenameQ = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Q03152022.mat');
            filenameTrigger = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Trigger03152022.mat');
            filenameX = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\X03152022.mat');
            % Load modified X scan data
            pathHeaderPreload = 'C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\';% local path on Xiaomeng's PC
            filenameCh3s_movMean = ([pathHeaderPreload 'X03152022_moveMean.mat']);
            calibrationAuto = 0;
            thresholdMag =  8;
            interferenceBandFreq = 25e6; 
            opticalPathDly = 1.5;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2)/c*alpha_up/1e6 + (opticalPathDly*2)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-6e6 6e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-6e6 6e6];
        case 2
            filenameI = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\I03232022_wall.mat');
            filenameQ = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Q03232022_wall.mat');
            filenameTrigger = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Trigger03232022_wall.mat');
            filenameX = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\X03232022_wall.mat');
            % Load modified X scan data
            pathHeaderPreload = 'C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\';% local path on Xiaomeng's PC
            filenameCh3s_movMean = ([pathHeaderPreload 'X03232022_wall_moveMean.mat']);
            calibrationAuto = 0;
            thresholdMag =  8;
            interferenceBandFreq = 25e6; 
            opticalPathDly = 1.5;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2)/c*alpha_up/1e6 + (opticalPathDly*2)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-6e6 6e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-6e6 6e6];
        case 3
            filenameI = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\I03232022_tripods.mat');
            filenameQ = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Q03232022_tripods.mat');
            filenameTrigger = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\Trigger03232022_tripods.mat');
            filenameX = ('C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\X03232022_tripods.mat');
            % Load modified X scan data
            pathHeaderPreload = 'C:\Users\xgao\Box\Workspace\CoherentLidar\HiWire\HiWire_data\';% local path on Xiaomeng's PC
            filenameCh3s_movMean = ([pathHeaderPreload 'X03232022_tripods_moveMean.mat']);
            calibrationAuto = 0;
            thresholdMag =  8;
            interferenceBandFreq = 25e6; 
            opticalPathDly = 1.5;
            disp(['Reference beat tone is ', num2str((rangeRef(n)*2)/c*alpha_up/1e6 + (opticalPathDly*2)/c*alpha_up/1e6), 'MHz'])

            maskFreqsUpChirp = [-6e6 6e6]; % masking frequency upper bound
            maskFreqsDnChirp = [-6e6 6e6];
        otherwise
            disp('Wrong entry, please enter 1 - 3')
    end
    disp(['Reference range is ', num2str(rangeRef(n)), 'm'])

    % Raw data loading
    load (filenameI) % Ch1s
    load (filenameQ) % Ch2s
    load (filenameTrigger) %Ch4s

    % Scanner position data loading and decimation
    load (filenameX); % Ch3s

    % Load modified X scan data
    disp('Loading moving average of X scan data...')
    load(filenameCh3s_movMean)

    % Load calibration dataset, either pre-calculated or runtime cal
%     samplesOffsetScanner = 0;
    samplesOffsetScanner = 0.15e-3 * fs;

    %% Initialize data
    % I and Q signal with tail portion chopped, matching delay fix
    I_Raw = Ch1s(1:end-samplesOffsetScanner);
    Q_Raw = Ch2s(1:end-samplesOffsetScanner);
    clear Ch1s Ch2s;

    % Find min on drive signal to define tLaser
    switch n
        case 1
            driveSig = -Ch4s(1:end-samplesOffsetScanner); % Trigger signal with tail portion chopped, matching delay fix
        case 2 % noticed beat tone is negative, could be wrong connection or so. Flipping drive signal may fix this.
            driveSig = -Ch4s(1:end-samplesOffsetScanner); % Trigger signal with tail portion chopped, matching delay fix            
        case 3 % noticed beat tone is negative, could be wrong connection or so. Flipping drive signal may fix this.
            driveSig = Ch4s(1:end-samplesOffsetScanner); % Trigger signal with tail portion chopped, matching delay fix            
        otherwise
            disp('Wrong entry, please enter 1-3')
    end
    clear Ch4s;
    N = length(driveSig);

    t = 0:1/fs:(N-1)/fs; 

    [minVal, tLaser] = min(driveSig(1:20000));
    if do_infoPlot
        figure
        subplot(211)
        plot(t*1e6, driveSig); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
        subplot(212)
        plot(t(1:20000)*1e6, driveSig(1:20000)); xlabel('time(us)'); ylabel('voltage(v)'); title('Drive signal')
    end
    clear driveSig;
    
% X and Y signal with head portion chopped, to fix delay
    rawPosX = Ch3s_movMean;
    rawPosY = zeros(1,length(rawPosX));
    clear Ch3s Ch3s_movMean;

% % %     if do_infoPlot
% % %         figure
% % %         subplot(231); plot(rawPosX); xlabel('samples'); ylabel('X position');title('raw position X');
% % %         subplot(232); plot(rawPosY); xlabel('samples'); ylabel('Y position');title('raw position Y');
% % %         subplot(233); plot(rawPosX, rawPosY,'o');xlabel('x'); ylabel('y'); title('scanning pattern')
% % %         hold on; plot(rawPosX(1), rawPosY(1), 'ro')
% % %         subplot(234); plot(posXdecimated); xlabel('samples'); ylabel('X position');title('decimated position X');
% % %         subplot(235); plot(posYdecimated); xlabel('samples'); ylabel('Y position');title('decimated position Y');
% % %         subplot(236); plot(posXdecimated, posYdecimated,'o');xlabel('x'); ylabel('y'); title('scanning pattern(post-decimation)')
% % %         hold on; plot(posXdecimated(1), posYdecimated(1), 'ro')
% % %     end

    %% FFT 
    % Perform segmented FFT, calculate range and velocity
    sigMx = I_Raw + 1i*Q_Raw;
    sigMxSeg = sigMx(tLaser:end); % chop off head portion before tLaser 
    tSeg = t(tLaser:end);
    numChirpCyc = length(sigMxSeg)/(tUpChirp+tDownChirp)/fs;

    % converting delay time to samples
    t0pSps = round(t0p*fs);
    t1pSps = round(t1p*fs);
    t2pSps = round(t2p*fs); % fft up-chirp
    t3pSps = round(t3p*fs);
    t0nSps = round(t0n*fs);
    t1nSps = round(t1n*fs);
    t2nSps = round(t2n*fs); % fft down-chirp
    t3nSps = round(t3n*fs);
    tSpsFullCycle = t0pSps + t1pSps + t2pSps + t3pSps + t0nSps + t1nSps + t2nSps + t3nSps;

    mkrFFTupS = t0pSps+t1pSps+1;
    mkrFFTupE = mkrFFTupS+t2pSps;
    mkrFFTdnS = mkrFFTupE+t3pSps+t0nSps+t1nSps+1;
    mkrFFTdnE = mkrFFTdnS+t2nSps;

    delaySpsStartup = t0pSps+t1pSps;
    delaySpsUp2Dn = t3pSps+t0nSps+t1nSps;
    delaySpsDn2Up = t3nSps+t0pSps+t1pSps;

    numSpsTotal = length(sigMxSeg);
    numSpsFFT = fftSize;
    f = -numSpsFFT/2:numSpsFFT/2-1;     % Specify horinzontal axis index
    f = f/numSpsFFT*fs;                 % convert in frequency
    % Define how many cycles (10us chirps) within each segment
    cycleIdx = 1:tSpsFullCycle:numSpsTotal;
    cycleLen = length(cycleIdx)-1;


%     Verification plot - Highlight delayed and valid FFT regions for verification
%     figure(4)
%     subplot(211)
%     plot(tSeg*1e6,real(sigMxSeg), 'color', [0.4940 0.1840 0.5560]); hold on;
% %     plot(t*1e6, driveSig,'c'); xlabel('time(us)'); ylabel('voltage(v)');
%     xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (real) with highlighted FFT regions')
%     subplot(212)
%     plot(tSeg*1e6,imag(sigMxSeg),'color' ,[0.4660 0.6740 0.1880]); hold on;
% %     plot(t*1e6, driveSig,'c'); xlabel('time(us)'); ylabel('voltage(v)'); 
%     xlabel('Time (us)'); ylabel('Amplitude (v)'); title('Mixed signal (imag) with highlighted FFT regions')

% pre-allocate space, including slide FFT extra points, speeds things up
    fp = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT)); 
    fpMag = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT));
    posXdecimated = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT)); 
    posYdecimated = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT)); 
    %% Sliced FFT
    for i = 1:length(cycleIdx)-1
    % Initialize variables
        
        fftBlkUpChirp = zeros(1,t2pSps); % pre-allocate space for fft chunks (total len = 3333)
        fftBlkUpChirpIdx = zeros(1,t2pSps);
        RunningCtrUp = 1;
        sigMx_subSet = sigMxSeg(cycleIdx(i):cycleIdx(i+1)-1);
        for j = 1:length(sigMx_subSet)
            if j >= mkrFFTupS && j < mkrFFTupE
                fftBlkUpChirp(RunningCtrUp) = sigMx_subSet(j);
                fftBlkUpChirpIdx(RunningCtrUp) = cycleIdx(i)+j-1;
                RunningCtrUp = RunningCtrUp + 1;
            end
        end
%             figure(4)
%             subplot(211)
%             plot(tSeg(fftBlkUpChirpIdx(1):fftBlkUpChirpIdx(end))*1e6, real(fftBlkUpChirp),'color',[0.85,0.33,0.10]); 
%             legend('Mixed signal - I', 'FFT region')



%       % Default MATLAB fft with zero-padding at the end
% % %         yFFTupChirp = fftshift(fft(fftBlkUpChirp, fftSize));

%       % slide FFT on available points
        yFFTupChirp_slideFFT = zeros(fftSize, numFFTPtPerCycle_slideFFT); % logging fftshifted FFTs
        S_raw = zeros(fftSize, numFFTPtPerCycle_slideFFT); % logging raw FFTs
        % Processing and Masking for the 1st FFT
        S_raw(:,1) = fft(fftBlkUpChirp(fftSegIdx(1):fftSegIdx(1)+fftSize-1)); % a full FFT segment runs from firstIdx : firstIdx+fftSize-1 (so total L = fftSize)
        yFFTupChirp_slideFFT(:,1) = fftshift(S_raw(:,1));
        maskIdxUpChirp = round(maskFreqsUpChirp/fs * fftSize);
        if do_masking
            yFFTupChirp_slideFFT(fftSize/2+maskIdxUpChirp(1) : fftSize/2+maskIdxUpChirp(2),1) = 0;
        end 
        % Processing and Masking for the rest slide FFTs
        for p = 2:length(fftSegIdx)
            S_raw(:,p) = (S_raw(:,p-1) - fftBlkUpChirp(p-1) + fftBlkUpChirp(p+fftSize-1)).*slideFFTcoeff;
            yFFTupChirp_slideFFT(:,p) = fftshift(S_raw(:,p));
            if do_masking % Masking for remaining slide FFTs
                yFFTupChirp_slideFFT(fftSize/2+maskIdxUpChirp(1) : fftSize/2+maskIdxUpChirp(2),p) = 0;
            end       
        end
%%---------------------------------------------------------------------%%
% there are more than 1 upchirp ffts, thus yFFTupchirp can no longer be used, need to allocate space = extrapoints_slideFFT

% % % % -Reference-  % Smart slide FFT 
% % %     S_raw = zeros(fftSize, length(fftSegIdx));
% % %     S = zeros(fftSize, length(fftSegIdx));
% % % %     S_raw(:,1) = fft(w.*complexSig_IQs(fftSegIdx(1):fftSegIdx(1)+fftSize-1)); % first fftSize-long dataset req. fft as prior knowledge
% % %     S_raw(:,1) = fft(complexSig_IQs(fftSegIdx(1):fftSegIdx(1)+fftSize-1)); % first fftSize-long dataset req. fft as prior knowledge
% % %     S(:,1) = fftshift(S_raw(:,1));
% % %     for p = 2:length(fftSegIdx)
% % %         S_raw(:,p) = (S_raw(:,p-1) - complexSig_IQs(p-1) + complexSig_IQs(p+fftSize-1)).*slideFFTcoeff;
% % %         S(:,p) = fftshift(S_raw(:,p));
% % %     end

% -Reference-
%%---------------------------------------------------------------------%%

        for p = 1:length(fftSegIdx)
            % Peak detection
            [fpMagCur, fpIdx] = max(abs(yFFTupChirp_slideFFT(:,p)));
            fp((i-1)*numFFTPtPerCycle_slideFFT+p) = f(fpIdx);
            fpMag((i-1)*numFFTPtPerCycle_slideFFT+p) = fpMagCur;
            posXdecimated((i-1)*numFFTPtPerCycle_slideFFT+p) = rawPosX(tLaser+(i-1)*tSpsFullCycle+delaySpsStartup+p);
            posYdecimated((i-1)*numFFTPtPerCycle_slideFFT+p) = rawPosY(tLaser+(i-1)*tSpsFullCycle+delaySpsStartup+p);
            % FFT plot and verification of masking band
            if do_FFTplot == 1
                figure(5) % original spectrum
                plot(f/1e6, abs(yFFTupChirp_slideFFT(:,p))); hold on; 
                xlabel('Frequency (MHz)'); ylabel('Magnitude'); title('Up-chirp FFT of all cycles')
    
            end
        end
        fftBlkUpChirp = [];
        fftBlkUpChirpIdx = [];

    end

    %% Perform thresholding, then Calculate SNR, Range & velocity based results
    rangeEst = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT)); % pre-allocate space, speeds things up
    fpOut = zeros(1,cycleLen*(numFFTPtPerCycle_slideFFT)); % output beat tone with threshold applied

    %% Range and Velocity Cal
    for i = 1:length(fp)
        fpCur = fp(i);
        fpMagCur = fpMag(i);

        if fpMagCur > thresholdMag && abs(fpCur) < interferenceBandFreq % Thresholding & interference removal
            fpOut(i) = fpCur;

            % Range calculation
            rangeCur = fpCur*c/2/alpha_up - opticalPathDly; % use only upchirp beat tone to get range
%             rangeCur = ((fpCur - fnCur)*c/2/alpha_up - opticalPathDly)/2; % considering optical length differences
%             rangeCur = (fpCur - fnCur)*c/4/alpha_up - opticalPathDly; % *new cal* considering optical length differences
            rangeEst(i) = rangeCur;
        else
            fpOut(i) = NaN;
            rangeEst(i) = NaN;    
        end

    end
    
    if do_EstResultDisplay
        disp('--------Estimation Results--------')
        disp(['Segment # ', num2str(segNum)])
        disp(['Estimated range is ', num2str(rangeEst), 'm'])
        disp(['Mean range is ', num2str(mean(rangeEst)), 'm'])    
        disp(['Mean, Median, std fp is ', num2str(mean(fpOut)/1e6), ' ', num2str(median(fpOut)/1e6), ' ', num2str(std(fpOut)/1e6), ' MHz'])
    end
   
    %% Distribution plots

    % Histogram of beat tone
    if do_infoPlot
        binWidth = fs/fftSize/1e6;
        figure
        subplot(121);histogram(fp/1e6,'BinWidth',binWidth,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution'); xlim([-22 22])
        subplot(122);histogram(fpOut/1e6,'BinWidth',binWidth,'EdgeColor',[1 1 1])
        xlabel('Frequency (MHz)');title('Up-chirp FFT beat tone distribution with thresholding')
    end
    %% Range and velocity estimation results
    if do_infoPlot
        figure
        plot(rangeEst,'o', 'MarkerFaceColor', 'b'); xlabel('cycle#'); ylabel('range(m)');title('Range Estimation')
        ylim([0 10])
    end
    %% Scatter plot
    L = min(length(posXdecimated), length(rangeEst));
    
    % 3D scatter plot
    markerSize = 10;
    % old 
    scatter_x = posXdecimated(1:L)/max(posXdecimated)*15;
    scatter_y = posYdecimated(1:L)/max(posXdecimated)*15;
    % new
%     scatter_x = (posXdecimated(1:L) - mean(posXdecimated(1:L)))/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_y = (posYdecimated(1:L) - mean(posXdecimated(1:L)))/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_x = (posXdecimated(1:L) - 0.01)/((max(posXdecimated(1:L))-min(posXdecimated(1:L)))/2)*15;
%     scatter_y = (posYdecimated(1:L) - 0.01)/(abs(min(posXdecimated(1:L)))/2)*15 - 5.5;

    scatter_rng = rangeEst(1:L);

    scatter_x_meter = sin(scatter_x/180*pi).*(rangeEst(1:L)).*cos(scatter_y/180*pi);
    scatter_y_meter = sin(scatter_y/180*pi).*(rangeEst(1:L));
    scatter_rng_meter = cos(scatter_x/180*pi).*(rangeEst(1:L)).*cos(scatter_y/180*pi);

    if do_infoPlot
        figure
        sSeg = scatter3(scatter_x,scatter_rng,scatter_y,markerSize,scatter_rng, 'filled'); % use velocity as color scale to show dots of range 3D map
        title(['Range estimation (seg #', num2str(segNum),')'])
        xlabel('X scan'); ylabel('Range (m)'); zlabel('Y scan')
        colorbar
        colormap(redblue(20))
        view(20, 40)
    end

    %% Spectrum plot
    if do_infoPlot
        figure
        subplot(121); plot(fp/1e6, fpMag,'o'); xlabel('Frequency (MHz)'); ylabel('Up-chirp'); title('w/o thresholding')
        subplot(122); plot(fpOut/1e6, fpMag,'go'); xlabel('Frequency (MHz)'); title('w/ thresholding');% xlim([-fs/2 fs/2-1]);
    end

    % Logging all results
    outputX(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_x';
    outputY(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_y';
    outputRange(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_rng;
  
    outputX_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_x_meter';
    outputY_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_y_meter';
    outputRange_meter(outputPtIdx_CurSegStart+1:outputPtIdx_CurSegStart+L) = scatter_rng_meter';
    
    % update index start to total length of current segment
    outputPtIdx_CurSegStart = outputPtIdx_CurSegStart + L;

% % %     if do_saveOutputData == 1
% % %         outputClusterFilename =['saveOutput_Seg',num2str(segNum),'.mat'];
% % %         save(outputClusterFilename,'snrUp','snrDn','snrUpdB','snrDndB','fp','fn', ...
% % %             'fpMag','fnMag','fpOut','fnOut','rangeEst','vEst','outputX','outputY',...
% % %             'outputRange','outputVelocity');
% % %     end
end

% removing residual empty space allocated for outputs. 
% outputPtIdx_CurSegStart after 15 segment loops is not at end of valid
% output index
outputX = outputX(1:outputPtIdx_CurSegStart);
outputY = outputY(1:outputPtIdx_CurSegStart);
outputRange = outputRange(1:outputPtIdx_CurSegStart);
outputX_meter = outputX_meter(1:outputPtIdx_CurSegStart);
outputY_meter = outputY_meter(1:outputPtIdx_CurSegStart);
outputRange_meter = outputRange_meter(1:outputPtIdx_CurSegStart);


% figure
% scatter3(-outputX,outputRange,outputY,5,outputVelocity, 'filled'); % use velocity as color scale to show dots of range 3D map
% title('Range & velocity estimation')
% xlabel('X scan (deg)'); ylabel('Range (m)'); zlabel('Y scan (deg)')
% colorbar
% colormap(redblue(20))
% view(20, 40)
% title('Range vs. velocity 4D scatter plot')
% ylim([0.1 10])

figure
scatter3(outputX,outputRange,outputY,5,outputRange, 'filled'); % use range as color scale to show dots of range 3D map
title('Range estimation')
xlabel('X scan (deg)'); ylabel('Range (m)'); zlabel('Y scan (deg)')
colorbar
% colormap(redblue(20))
view(20, 40)
title('Range 3D scatter plot (deg)')
% caxis([-5 10])
ylim([0.1 10])

%% Convert to meter scale

figure
scatter3(outputX_meter,outputRange_meter,outputY_meter,10,outputRange_meter, 'filled')
xlabel('x(m)')
ylabel('range')
zlabel('y(m)')
% xlim([-2 2])
ylim([0 10])
% zlim([-2 2])
title('Range 3D scatter plot (meter)')

timeCostTotal = toc;
disp(['---- Total time cost is ', num2str(timeCostTotal/60), 'min ----'])

%% Estimate # points corresponding to target
% HitDetRangeBoundLo = floor(rangeRef(n));
% HitDetRangeBoundHi = floor(rangeRef(n)+1);
HitDetRangeBoundLo = 6;
HitDetRangeBoundHi = 8;

hitIdx_LargerThanBoundLo = find(outputRange > HitDetRangeBoundLo);
hitIdx_SmallerThanBoundHi = find(outputRange < HitDetRangeBoundHi);
hitIdx_intersect = intersect(hitIdx_SmallerThanBoundHi,hitIdx_LargerThanBoundLo);
disp(['# hit points on target within the expected range (', num2str(HitDetRangeBoundLo),'~',num2str(HitDetRangeBoundHi), ') are ', num2str(outputRange_meter(hitIdx_intersect)),'m']);
disp(['# hits = ', num2str(length(hitIdx_intersect))]);