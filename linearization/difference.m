function diff = difference(input)
N = length(input);
for i = 2:N
    diff(i) = input(i)-input(i-1);
end
diff(1) = diff(2);
end