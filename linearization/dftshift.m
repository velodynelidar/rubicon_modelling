function [X,f]=dftshift(x,Fs,varargin) 


%**********************
%[X,f]=dftshift(x,Fs,varargin) 
%Calculates DFT and scales frequency axis based on sampling freq 
%to give actual frequency.
%Input
%x: input data
%Fs: sampling freq in Hz
%varargin : number of point optional, if not stated it will use lenght ox
% Output
%X fft of inputshiftted to give symmetric axis
%f freq in Hz
%**********************

Ts=1/Fs;  % sampling time. 

optargin = size(varargin,2);  % number of optional arguments
%stdargin = nargin - optargin;

if optargin >= 1 
    N=cell2mat(varargin(1));
    %fprintf('     %d\n', Fs)
else
    N=length(x);
end


X=fftshift(fft(x,N));

f=-N/2:N/2-1;
f=f* Fs/N;   % freq resolution is Fs/N 

return




