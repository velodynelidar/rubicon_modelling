function chirp = running_sum(freq,tau,fs,offset)
N = length(freq)-2*offset;%skip initial and end samples
chirp = zeros(1,N);
chirp(1) = freq(offset+1);
for i = 2:N
    chirp(i) = freq(offset+i)+chirp(i-1);
end
chirp = chirp/(tau*fs);
end
