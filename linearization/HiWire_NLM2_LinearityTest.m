clear
load('newNLM_DriveSignal.mat')
figure(1); clf
tiledlayout('flow');
sgtitle('Data from Short DLI')
figure(2); clf
tiledlayout('flow');
sgtitle('Data from Long DLI')
figure(3); clf
%% Inputs

NumIterations = 20; % Number of times code will loop through 
% This could be set to reach a specific nonlinearity, BTW, etc.

% Instrument settings
t_chirp = 5e-6; % Chirp duration (1/2 cycle)
AWG_SampleRate = 12e9;  % AWG DAC rate
Fs = 1e9;   % Undecimated sampling rate of oscilloscope
RL = 20e3; % Uncropped record length of oscilloscope
% Make sure the Delay on the scope is set properly

% DLI settings
delay = 1.875/2; % Path length in short DLI. Experimentaly determined.
c = 3e8/1.5;    % Speed of light in fiber
tau = delay/c;  % Delay time in short DLI

%B = 950e6;  % Expected chirp bandwidth of laser. 
% Maybe better if this is dynamicaly measured.

% Modulation waveform settings
driver_gain = 1;    % Fraction of AWG full swing to use. (Full swing set in AWG soft front panel.)
n_smooth = 1; % Smoothing factor when generating new drive functions
CorrectionGain = 0.1; % Fraction of error value used in creating next drive signal
cropPercent = 0.05; % Fraction of estimated laser chirp to ignore when computing nonlinearity


%% Connect to scope
scope =  instrfind('Type', 'visa-usb', 'RsrcName', 'USB0::0x0699::0x0503::C051301::0::INSTR', 'Tag', '');

% Create the VISA-TCPIP object if it does not exist
% otherwise use the object that was found.
if isempty(scope)
%     scope = visa('NI', 'TCPIP0::192.168.1.2::inst0::INSTR');
    scope = visa('KEYSIGHT', 'USB0::0x0699::0x0503::C051301::0::INSTR');
    
else
    fclose(scope);
    scope = scope(1);
end

% Configure instrument object, obj1
set(scope, 'InputBufferSize', RL);
set(scope, 'OutputBufferSize',RL);
fopen(scope);

fprintf(scope,'HORIZONTAL:MODE MAN');% set the horizontal mode. Manual mode lets you change sample mode and record length
fprintf(scope,'HORIZONTAL:MODE:RECORDLENGTH %f',RL);
% RL = str2double(query(scope, 'HORizontal:ACQLENGTH?')); % Query the record length
fprintf(scope, sprintf('HORizontal:MODE:SAMPLERate %f', Fs));   %   Set sampling rate
% Fs= str2double(query(scope, 'HORizontal:DIGital:SAMPLERate:MAIN?'));
% HORizontal:SAMPLERate
fprintf(scope, 'ACQuire:STOPAFTER SEQUENCE'); % tell scope to take a single when acquiring

% Take constant stuff
fprintf(scope, 'DATa:SOUrce CH1');
fprintf(scope, 'DATa:ENCdg RPBinary');
fprintf(scope, 'DATa:WIDth 1');

fprintf(scope, 'DATa:STARt 0');
fprintf(scope, sprintf('DATa:STOP %.f', RL));
xincr = str2double(query(scope, 'WFMPRE:XINCR?')); % this is same for all channels
ymult_Ch1 = str2double(query(scope, 'WFMPRE:YMULT?'));  % we need to retrive these only once.
yzero_Ch1 = str2double(query(scope, 'WFMPRE:YZERO?'));
yoff_Ch1 = str2double(query(scope, 'WFMPRE:YOFF?'));

fprintf(scope, 'DATa:SOUrce CH2');
ymult_Ch2 = str2double(query(scope, 'WFMPRE:YMULT?'));
yzero_Ch2 = str2double(query(scope, 'WFMPRE:YZERO?'));
yoff_Ch2 = str2double(query(scope, 'WFMPRE:YOFF?'));

fprintf(scope, 'DATa:SOUrce CH3');
ymult_Ch3 = str2double(query(scope, 'WFMPRE:YMULT?'));
yzero_Ch3 = str2double(query(scope, 'WFMPRE:YZERO?'));
yoff_Ch3 = str2double(query(scope, 'WFMPRE:YOFF?'));

fprintf(scope, 'DATa:SOUrce CH4');
ymult_Ch4 = str2double(query(scope, 'WFMPRE:YMULT?'));
yzero_Ch4 = str2double(query(scope, 'WFMPRE:YZERO?'));
yoff_Ch4 = str2double(query(scope, 'WFMPRE:YOFF?'));

Ch1 = zeros(RL,1);
Ch2 = zeros(RL,1);
Ch3 = zeros(RL,1);
Ch4 = zeros(RL,1);

%% Generate the first laser current modulation drive signal on the AWG
numSamplesChirp = t_chirp*AWG_SampleRate;

% generate triangle waveform
[DAC_wfm, rpt, ~] = iqpulsegen('sampleRate', AWG_SampleRate, ...
              'pw', 0, 'rise', 60000, 'fall', 60000, ...
              'off', 0, 'low', -1, 'high', 1, ...
              'pulseShape', 'Trapezodial', 'alpha', 5, 'correction', 0, 'channelMapping', [1 0; 0 0]);

RampLength = length(DAC_wfm)/2;

% Set trigger for timing purposes
[trigger, ~, ~] = iqpulsegen('sampleRate', AWG_SampleRate, ...
              'pw', 60000, 'rise', 0, 'fall', 0, ...
              'off', 60000, 'low', -0.5, 'high', 0.5, ...
              'pulseShape', 'Trapezodial', 'alpha', 5, 'correction', 0, 'channelMapping', [0 0; 1 0]);

iqdownload(repmat(trigger, rpt, 1), AWG_SampleRate, 'channelMapping', [0 0; 1 0], 'segmentNumber', 1);


%% Initialize arrays

DAC_wfms= zeros(NumIterations+1,length(DAC_wfm)); % Initialize array to store DAC waveforms
DAC_wfms(1,:) = movmean(DAC_wfm,n_smooth);   % Save first waveform into array, with smoothing

Is = zeros(NumIterations, RL);  % Initialize array to store all the I data (short DLI)
Qs = zeros(NumIterations, RL);  % Initialize array to store all the Q Data (short DLI)
Reals = zeros(NumIterations, RL);   % Initialize array to store all the data from long DLI
Trigs = zeros(NumIterations, RL);   % Initialize array to store the trigger data

IFs = zeros(NumIterations, RL-1); % Initialize array to store the estimated IFs 
las_chirps = zeros(NumIterations, RL-1); 

% More metrics need to be here
error1stds = zeros(NumIterations,1);
error2stds = zeros(NumIterations,1);
error3stds = zeros(NumIterations,1);

%% Initialize figure
figure(1);


%% Loop
for i=1:NumIterations
    i
   %% Acquire data
    % download voltramp to AWG
%     iqdownload(repmat(driveSigRaw_normalizedResampled, rpt, 1), 12e9, 'channelMapping', [1 0; 0 0], 'segmentNumber', 1, 'normalize', 1);
    iqdownload(repmat(DAC_wfm, rpt, 1), 12e9, 'channelMapping', [1 0; 0 0], 'segmentNumber', 1, 'normalize', 1);
   
    pause(1)  
   
     % take single
    fprintf(scope, 'ACQuire:STATE RUN'); 
    pause(0.1)

    % Acquire Channel 1 
    fprintf(scope, 'DATa:SOUrce CH1');
    fprintf(scope, 'CURVE?');  
    Ch1_Raw = binblockread(scope);
    Ch1 = (Ch1_Raw - yoff_Ch1) * ymult_Ch1 + yzero_Ch1;
    Ch1 = Ch1 - mean(Ch1);
    Is(i,:) = Ch1;

    % Acquire Channel 2
    fprintf(scope, 'DATa:SOUrce CH2');
    fprintf(scope, 'CURVE?');  
    RawCh2 = binblockread(scope);
    Ch2(:,1) = (RawCh2 - yoff_Ch2) * ymult_Ch2 + yzero_Ch2;
    Ch2 = Ch2 - mean(Ch2);
    Qs(i,:) = Ch2;
    
    % Acquire Channel 3 
    fprintf(scope, 'DATa:SOUrce CH3');
    fprintf(scope, 'CURVE?');  
    Ch3_Raw = binblockread(scope);
    Ch3 = (Ch3_Raw - yoff_Ch3) * ymult_Ch3 + yzero_Ch3;
    Ch3 = Ch3 - mean(Ch3);
    Reals(i,:) = Ch3;

    % Acquire Channel 4
    fprintf(scope, 'DATa:SOUrce CH4');
    fprintf(scope, 'CURVE?');
    RawTrig = binblockread(scope);
    Trig(:,1) = (RawTrig - yoff_Ch4) * ymult_Ch4 + yzero_Ch4;
    Trig = Trig - mean(Trig);
    Trigs(i,:) = Trig(:,1);
    
    %% Get the BeatFreqs using IF method
    I = Is(i,:);
    Q = Qs(i,:);
    I = I/rms(I);
    Q = Q/rms(Q);
    
    x = I+1j*Q;
    mean_N = 100;   % I need to refresh on the impact of this value
    kernel = ones(1,mean_N)/mean_N;
    xint = conv(x,kernel);
    x1 = xint(mean_N/2:end-1-mean_N/2);
%     x1 = xint(mean_N/2+1:end-mean_N/2);
    phase = unwrap(angle(x1));
    IF = ((difference(phase)).')*Fs/(2*pi); % Instantaneous frequency estimation
    
    % plot IF estimates
    figure(1)
    nexttile(1); hold on
    plot((0:length(IF)-1)/Fs,IF);
    title('Instantaneous Frequency Estimation')
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    
    %% Estimate the laser chirp
    IF = movmean(IF,200);
    las_chirp = running_sum(IF, tau, Fs, 0);   % running sum of IF to estimate laser chirp
    las_chirps(i,:)= las_chirp;
    
    % find the measured Bandwidth
    BW = max(las_chirp)-min(las_chirp);
    BWs(:,i) = BW;
    
    % plot laser chirp estimate
    nexttile(2); hold on
    plot((0:length(IF)-1)/Fs,las_chirp)
    title('Laser Chirp Estimate')
    xlabel('Time(s)')
    ylabel('Frequency(Hz)');
    
    %% Extract chirps
    
    len_ChirpRamp = floor(1*5e-6*Fs);

    % find a valley
    [temp, temp_k] = min(las_chirp(6000:12000));
    temp_k = temp_k + 6000;
    % TODO improve the valley search function using trigger signal
    
    % slice out a positive chirp and negative chirp
    posChirp = las_chirp(temp_k:temp_k+len_ChirpRamp);
    negChirp = las_chirp(temp_k - len_ChirpRamp:temp_k);
    
    % drop off edges of chirp when computing error and nonlinearity
    cropAmount = floor(length(negChirp)*cropPercent);
    posChirp = posChirp(cropAmount+1:end-cropAmount);
    negChirp = negChirp(cropAmount+1:end-cropAmount);
    
    %% Compute Error
    
    % find linear fit
    
    % if i == 1    % Uncomment this line if reference should be found only on first iteration
        posFit_polyVals = polyfit([1:length(posChirp)], posChirp,1);
        posFit = polyval(posFit_polyVals,[1:length(posChirp)]);
%         posFit = linspace(min(posChirp),max(posChirp),length(posChirp));

        negFit_polyVals = polyfit([1:length(negChirp)], negChirp,1);
        negFit = polyval(negFit_polyVals,[1:length(negChirp)]);
%         negFit = linspace(max(negChirp),min(negChirp),length(negChirp));
%     end

    % Compute residual errors
    posError = posChirp - posFit;
    negError = negChirp - negFit;
    
    posErrors(:,i) = posError;
    negErrors(:,i) = negError;
    
    % Plot residual errors
    nexttile(3); cla reset; hold on; grid on;
    plot(posError)
    plot(negError)
%     ylim([-1e7 1e7])
    title('Residual Error')
    xlabel('Samples')
    ylabel('Frequency Error (Hz)')
    legend('Positive Chirp','Negative Chirp','Location','best')
    
    
    posNonlinearities(:,i) = std(posError)/(BW*(1-2*cropPercent))*100;
    negNonlinearities(:,i) = std(negError)/(BW*(1-2*cropPercent))*100;
        
    % plot nonlinearity
    nexttile(4); hold on; grid on;
    plot(i, posNonlinearities(:,i),'bo', 'MarkerFaceColor', 'b');
    plot(i, negNonlinearities(:,i),'ro', 'MarkerFaceColor', 'r');
    title('Chirp Nonlinearity Estimation vs. Linearization Iteration')
    xlabel('Iteration #')
    ylabel('Nonlinearity %')
    legend('Positive Chirp', 'Negative Chirp')
    
    % plot frequency content of residual error
    nexttile(5); cla reset; hold on; grid on
    [posErrorFFT, ~] = dftshift(posError, Fs);
    posErrorFFT = abs(posErrorFFT);
    [negErrorFFT, freqs_error] = dftshift(negError, Fs);
    negErrorFFT = abs(negErrorFFT);
    plot(freqs_error, posErrorFFT, 'bo-')
    plot(freqs_error, negErrorFFT, 'ro-')
    title('Frequency Spectrum of Residual Error')
    xlabel('Frequency (Hz)')
    legend('Positive Chirp', 'Negative Chirp')
    xlim([-4e6 4e6])
    
    %% Make corrected pulse using appropriate gain
    
    % Scale the range and length of error arrays so that they apply to DAC range
    t_new = linspace(1, length(posError), RampLength*(1-2*cropPercent)); % Define scale for interpolation
    
    posError_scaled = interp1([1:length(posError)], posError, t_new);
    posError_scaled = posError_scaled/BW*CorrectionGain;
    posError_front = linspace(0, posError_scaled(1), RampLength*cropPercent);
    posError_end = linspace(posError_scaled(end), 0, RampLength*cropPercent);
    posError_cat = cat(2, posError_front, posError_scaled, posError_end);
    
    negError_scaled = interp1([1:length(negError)], negError, t_new);
    negError_scaled = negError_scaled/BW*CorrectionGain;
    negError_front = linspace(0, negError_scaled(1), RampLength*cropPercent);
    negError_end = linspace(negError_scaled(end), 0, RampLength*cropPercent);
    negError_cat = cat(2, negError_front, negError_scaled, negError_end);
    
    totError = cat(2, posError_cat, negError_cat);
    
    % apply scaled error to previous DAC waveforms
    DAC_wfm = DAC_wfm - totError'*2; % multiply by two because that is the range of the DAC
    
    % smooth out the drive waveform, ideally in a "circular" manner
    DAC_wfm = movmean(DAC_wfm([(end-floor(n_smooth./2)+1):end 1:end 1:(ceil(n_smooth./2)-1)]), n_smooth, 'Endpoints', 'discard');
    
    % circularly shift the waveform to ensure peak value is at center
    [~,max_k] = max(DAC_wfm);
    DAC_wfm = circshift(DAC_wfm, RampLength-max_k);    
    
    % save new DAC_wfm for next iteration
    DAC_wfms(i+1,:) = DAC_wfm;
    
    
    %% Use long DLI as metric
    delay = 550;    % counts in delay time
    
    % Take FFT in central region of up chirp
    [FFT_pos, ~] = dftshift(Ch3(delay+1000:delay+4000),Fs);
    FFT_pos = abs(FFT_pos);
    
    [FFT_neg, freqs] = dftshift(Ch3(delay+6000:delay+9000),Fs);
    FFT_neg = abs(FFT_neg);

    % plot FFTs
    figure(2)
    nexttile(1); hold on; grid on;
    plot(freqs, FFT_pos,'o-')
    xlim([50e6 200e6])
    title('FFT during 3us of Pos Chirp')
    xlabel('Frequency (Hz)')
    
    nexttile(2); hold on; grid on;
    plot(freqs, FFT_neg, 'o-')
    xlim([50e6 200e6])
    title('FFT during 3us of Neg Chirp')
    xlabel('Frequency (Hz)')
    
    % Compute KPIs on pos chirp
    posPeaks(:,i) = max(movmean(FFT_pos,3));
    negPeaks(:,i) = max(movmean(FFT_neg,3));
    
    nexttile(3); hold on; grid on;
    plot(i, posPeaks(:,i),'bo', 'MarkerFaceColor', 'b');
    plot(i, negPeaks(:,i),'ro', 'MarkerFaceColor', 'r');
    title('Peak Values of FFT (movmean=3)')
    xlabel('Iteration #')
    ylabel('Amplitude')
    legend('Positive Chirp', 'Negative Chirp', 'Location', 'best')
    
    posW3dbs(:,i) = sum(FFT_pos>0.5*posPeaks(:,i)/2)*Fs/3000;
    negW3dbs(:,i) = sum(FFT_neg>0.5*negPeaks(:,i)/2)*Fs/3000;
    
    nexttile(4); hold on; grid on;
    plot(i, posW3dbs(:,i),'bo', 'MarkerFaceColor', 'b');
    plot(i, negW3dbs(:,i),'ro', 'MarkerFaceColor', 'r');
    title('3dB Width of Peak')
    xlabel('Iteration #')
    ylabel('Frequency (Hz)')
    legend('Positive Chirp', 'Negative Chirp')
    
    
    posW10dbs(:,i) = sum(FFT_pos>0.1*posPeaks(:,i)/2)*Fs/3000;
    negW10dbs(:,i) = sum(FFT_neg>0.1*negPeaks(:,i)/2)*Fs/3000;
    
    nexttile(5); hold on; grid on;
    plot(i, posW10dbs(:,i),'bo', 'MarkerFaceColor', 'b');
    plot(i, negW10dbs(:,i),'ro', 'MarkerFaceColor', 'r');
    title('10dB Width of Peak')
    xlabel('Iteration #')
    ylabel('Frequency (Hz)')
    legend('Positive Chirp', 'Negative Chirp')
    
   
    
    
end
figure(1)
nexttile(4)
plot(posNonlinearities, 'b-')
plot(negNonlinearities, 'r-')

figure(2)
nexttile(3)
plot(posPeaks, 'b-')
plot(negPeaks, 'r-')

nexttile(4)
plot(posW3dbs, 'b-')
plot(negW3dbs, 'r-')

nexttile(5)
plot(posW10dbs, 'b-')
plot(negW10dbs, 'r-')


figure(3); hold on;
plot(DAC_wfms')
title('DAC Waveforms')
xlabel('Samples')

